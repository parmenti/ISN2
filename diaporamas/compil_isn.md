 <span class="menu-title" style="display: none">Titre</span>
![ESPE](images/logo_ul_espe_fond_transparent300dpi.png)<!-- .element: class="plain" style="width:60%;float:left;"--> 
![LORIA](images/logo_loria.png)<!-- .element: class="plain" style="width:12%;float:right;"--> 
<br><br><br><br><br><br>

---

**<br>Formation ISN - Langage et compilation<br>**

<br><br>_Mai 2019_<br><br><br>Yannick Parmentier



## Introduction
<br>
<p>**Langage** : <br>
  Système de signes doté d'une sémantique, et le plus souvent d'une syntaxe, permettant la communication.<span style="font-size:12pt;float:right;"><br>(source: [wikipedia](https://fr.wikipedia.org/wiki/Langage))</font></p>

<p class="fragment" data-fragment-index="2">
**Compilation** : <br>traduction d'un énoncé (code) d'un langage (source) vers un langage (cible)
</p>
<p class="fragment" data-fragment-index="3">
**Exemples** :
 <br>- compilation d'un programme C en un programme exécutable (langage assembleur, i.e., instructions *exécutables* par la machine)
</p>
<p class="fragment" data-fragment-index="3"
 <br>- compilation d'un programme Java en un fichier bytecode (.class) exécutable par une machine *virtuelle* (JVM)
</p>



## <br><br><br><br><br>Quelques notions de théorie des langages
---
<br> Classification des langages de Chomsky



## Langage
<br>
- Concrètement, qu'est ce qu'un langage ?

 $\rightarrow$ un ensemble (éventuellement infini) d'énoncés<!-- .element: class="fragment" data-fragment-index="2" -->

- Qu'est ce qu'un énoncé ?<!-- .element: class="fragment" data-fragment-index="3" -->

 $\rightarrow$ une suite (finie) de symboles (e.g., de caractères)<!-- .element: class="fragment" data-fragment-index="4" -->
 
 NB: on parle généralement de $mot$ plutôt que d'énoncé<!-- .element: class="fragment" data-fragment-index="4" -->

- Qu'est ce qu'un symbole ?<!-- .element: class="fragment" data-fragment-index="5" -->

 $\rightarrow$ une valeur appartenant à un ensemble appelé vocabulaire ou alphabet (défini arbitrairement) <!-- .element: class="fragment" data-fragment-index="6" -->



## Langage (suite)
<br>
Formellement : 

 - un *mot* $m$ sur un vocabulaire $V$ est une *chaîne* (concaténation) de longueur finie (éventuellement vide) de symboles de $V$.
 
 - l'ensemble des mots *constructibles* avec les symboles de V (on dit aussi l'ensemble des mots sur $V$) est noté $V^{*}$.

 - Un *langage* construit sur $V$ est un sous-ensemble de $V^{*}$.
 
 - Le *mot vide* est généralement noté $\varepsilon$.
 
Exemple :

 - Soit $V$ = { $a, b$ }, $m = aab$ est un mot de $V^{*}$.

 - $L$ = { $aab, aba, abb$ } est un langage sur $V$.



## Langage (suite)

- comment **représenter** un langage ?

  - en *énumérant* les mots le composant 
  
  - au moyen d'un *système de règles* (appelé *grammaire*) décrivant les mots du langage
  
- quelle est l'**expressivité** d'un langage ? 

  - lien avec la notion de *calculabilité*

- deux **tâches** (algorithmes) fondamentales :

  - déterminer si un mot appartient à un langage <br>(*reconnaissance*)
  
  - déterminer la(les) séquence(s) de règles permettant de construire un mot (*analyse* [syntaxique])



## Grammaire
<br>
Formellement, une grammaire $G$ est défini par un quadruplet $G = (V_T ,V_N ,S,P)$ où :

 - $V_T$ est le vocabulaire terminal (alphabet)
 
 - $V_N$ est le vocabulaire non terminal 

 - $S \in V_N$ est un symbole distingué appelé axiome
 
 - $P \subset (V_T \cup V_N)^{+} \times (V_T \cup V_N )^{∗}$ sont des règles de production
   <br><br>Les éléments de $P$ sont de la forme  $\alpha \rightarrow \beta$ <br>
   $$\mbox{avec } \alpha \in (V_T \cup V_N )^{+}, \beta \in (V_T \cup V_N)^{∗}$$
   et sont appelés des *règles de réécriture*



## Exemple de grammaire
<br>
$G_1 = (V_T ,V_N ,S,P)$

$V_T$ = {$a,b$}

$V_N$ = {$A,B,S$}

$S$ = $S$

$P = {S \rightarrow A B, A \rightarrow a, A \rightarrow a A, B \rightarrow \varepsilon,B \rightarrow b B}$

<br>Les mots suivants appartiennent au langage défini par $G_1$ :<br><br>
$$
a, aaa, ab, \dots
$$



## Dérivation
<br>
- Pour construire un mot du langage, on utilise la notion de *dérivation* (ou réécriture) noté $\Rightarrow_G$

- Soient la règle $\alpha \rightarrow \beta$ et les mots $\delta, \gamma$ tels que $$\alpha \in (V_T \cup V_N )^{+}, \beta, \delta \mbox{ et } \gamma \in (V_T \cup V_N)^{∗}$$ :

 <br>On note $$\delta \alpha \gamma \Rightarrow_G \delta \beta \gamma$$ <br>la dérivation (réécriture) du mot $\delta\alpha\gamma$ en $\delta\beta\gamma$ pour $G$
 
- On note $\Rightarrow_G^{*}$ la clotûre transitive de la relation de dérivation



## Langage défini par une grammaire
<br>
- On note $L(G)$ le langage défini par la grammaire $G$

- $L(G)$ correspond à l'ensemble des mots dérivables par G (à partir de l'axiome), ce que l'on note :

<center>
$L(G)$ = {$w ∈ V_T^{∗} \mid\ S \Rightarrow_G^* w$ }
</center>
<br>
- En reprenant la grammaire $G$ vue précédemment <br>($P = {S \rightarrow A B, A \rightarrow a, A \rightarrow a A, B \rightarrow \varepsilon,B \rightarrow b B}$),<br> on obtient :

 - $S \Rightarrow_G A B \Rightarrow_G a B \Rightarrow_G a$

 - $S \Rightarrow_G A B \Rightarrow_G a A B \Rightarrow_G a a A B \Rightarrow_G a a a B \Rightarrow_G a a a$



## Classification de Chomsky

- Classification des langages formels proposée par N. Chomsky :
 
  - grammaires de *type 0* : **langages récursivement énumérables** (aucune contrainte sur les règles)
  
  - grammaires de *type 1* : **langages contextuels** <br>les règles sont de la forme $\delta A \gamma \rightarrow \delta \beta \gamma$ avec $A \in V_N$, $\beta, \delta, \gamma \in (V_T \cup V_N)^{*}, \beta \neq \varepsilon$
  
  - grammaires de *type 2* : **langages algébriques (hors-contexte)** <br>les règles sont de la forme  $X \rightarrow \beta$ avec $X \in V_N$, $\beta \in (V_T \cup V_N)^{∗}$
  
  - grammaires de *type 3* : **langages rationnels (réguliers)** <br>les règles sont de la forme $A \rightarrow a B$ ou $A \rightarrow x$ avec
$A,B \in V_N$, $a,x \in V_T$



## Classification de Chomsky (suite)
<br>
On peut noter :

| Grammaire | | Outil formel de reconnaissance |
|:---|---|:---|
|Type 0| | Machine de Turing|
|Type 1| | Automate linéairement borné|
|Type 2| | [Automate à pile](https://www-verimag.imag.fr/~perin/enseignement/RICM3/infaeg/cours/RICM3%20-%20A&L%20-%20AUP%20-%20automate%20a%20une%20pile.pdf)|
|Type 3| | Automate a états fini|

<br>
$$ L^3 \subset L^2 \subset L^1 \subset L^0$$
<br><br>
<small>NB: il existe d'autres classifications des langages formels, incluant par exemple les langages légèrement sensibles au contexte.</small>



## Principales opérations sur les langages
<br>
- *Union* : <br>$L_1 \cup L_2$ désigne l'ensemble des mots appartenant à $L_1$ ou $L_2$

- *Intersection* : <br>$L_1 \cap L_2$ désigne l'ensemble des mots appartenant à $L_1$ et $L_2$

- *Complémentaire* : <br>$L^C$ désigne l'ensemble des mots construits sur l'alphabet de $L$ qui ne sont pas dans $L$

- *Concaténation* : $L_1.L_2$ = {$xy \mid x \in L_1, y \in L_2$}

- *Etoile de Kleene* : $L^*$ = {$\varepsilon \cup u_1.u_2 ... .u_n \mid u_i \in L\ \forall i > 0$}

- *Miroir* : $\overline{L}$ = {$u_1.u_2 ... u_n \mid u_n ... u_1 \in L$}



## Langages informatiques
<br>
- Les langages de programmation sont généralement définis au moyen :

  1. d'une grammaire régulière (type 3) pour ce qui est de l'analyse des *tokens* (unités lexicales incluant les mots-clés, identifiants / variables et valeurs / constantes du langage)

  2. d'une grammaire hors-contexte (type 2) pour ce qui est de l'analyse des *instructions* et *expressions* du langage



## <br><br><br><br><br><br> Langages réguliers
---



## Langages réguliers

- Les langages réguliers sont *clos* pour l'union, intersection, complémentaire, concaténation, étoile de Kleene et miroir

- Les langages réguliers ont une *expressivité limitée* (par exemple $L$ = {$a^n.b^n \mid n > 0$} défini sur $V$ = {$a,b$} n'est pas régulier)

- Un langage régulier peut être décrit par une **expression régulière** : 
  un mot construit sur $V$ = {$+,.,*,\varepsilon, \emptyset$} tel que
  
  - $\emptyset$ désigne le langage vide {$\emptyset$}
  - $\varepsilon$ désigne le langage {$\varepsilon$}
  - $x \in V$ désigne le langage {$x$}
  - $x+y$ désigne le langage $L_x \cup L_y$
  - $x.y$ désigne le langage $L_x.L_y$
  - $x^{*}$ désigne le langage $L$<small>&#xFF0A;</small>



## Expressions régulières
<br>

|Expression regulière | | Langage décrit|
|:---|---|:---|
|$a+b.c$ | |{$a,bc$}|
|$(a.b)^∗$|| {$\varepsilon,ab,abab,ababab,...$}|
|$a.b+c^∗$|| {$ab,c,cc,ccc,...$}|
|$x.y^∗.y+z.x+\varepsilon$ ||{$\varepsilon,zx,xy,xyy,xyyy,xyyyy,...$}|
|$a.a^∗.b.b^∗$| |{$ab,aaaaaaab,abbbbb,aaabbbbb,...$}|

A noter :
 - priorité entre opérateurs : $* > . > +$
 - $.$ est par ailleurs souvent omis



## Reconnaissance pour un langage régulier
<br>
- Utilise un **automate à états finis** (FSA en anglais)

- Formellement : un automate est un quintuplet $A = (Q,V,\delta,q_0,F)$ tel que:
 
  - $Q$ est un ensemble fini d'états
  - $V$ est un ensemble fini de symboles
  - $\delta \subseteq Q \times V \cup ${$\varepsilon$}$ \times Q$ est la relation de transition,
  - $q_0 \in Q$ est l'état initial
  - $F \subseteq Q$ est l'ensemble des états finaux

- Exemple :

$A=$ ($Q=$ {$q_0,q_1,q_2$}, $V=$ {$a,b$}, $\delta=$ {$(q_0,a,q_1)$, $(q_1,a,q_1)$, $(q_1,b,q_2)$}, $q_0= q_0$, $F=$ {$q_2$} )



## Reconnaissance pour un langage régulier (suite)
<br>
- Représentation graphique :

<center>
![](images/fsa.png)<!-- .element: class="plain" -->
</center>

(automate reconnaissant les mots décrits par l'expression régulière $aa^*b$ et par la grammaire $G$ = ($V_T$={$a,b$}, $V_N$={$S,X$}, $S$, $P$={$S\rightarrow a X, X \rightarrow b, X \rightarrow a X$})

- Propriété : pour tout automate à états finis reconnaissant un langage régulier, il existe un automate à états finis *déterministe* équivalent (i.e., reconnaissant le même langage)



## Reconnaissance pour un langage régulier (suite)
<br> 
- Un automate $A$ est dit déterministe si $\forall p, q, q' \in Q, \forall x \in V$ : $(p,x,q) \in \delta \wedge (p,x,q') \in \delta \Rightarrow q = q'$

- Tout automate à états finis peut être déterminisé : [[algorithme](http://www.momirandum.com/automates-finis/Determinisationdunautomate.html)]

- Soient $w$ un mot de $V$ tel que $w = x_0.x_1...x_n \in V^*$ et $A$ un automate tel que $A = (Q,V,\delta,q_0,F)$

- $w$ appartient au langage décrit par $A$ ssi

 - $\exists q_0.q_1 ... q_n.q_n$<sub><span style="font-size:12pt">$+$</span></sub>$_1$ $\in Q^*$
 
 - $\forall 0 \leq i \leq n, (q_i, x_i, q_i$<sub><span style="font-size:12pt">$+$</span></sub>$_1) \in \delta$
 
 - $q_n$<sub><span style="font-size:12pt">$+$</span></sub>$_1 \in F$



## Algorithme de reconnaissance 

<pre><code data-sample='code/automatelib.py#16-17,23-25,17'></code></pre>

<pre><code data-sample='code/automatelib.py#131-132,134-141,143-145,147-148'></code></pre>

<small style="float:right;">Source: [https://gist.github.com/NaPs/201493](https://gist.github.com/NaPs/201493)</small>



## Exercices (1/3)

<iframe class="stretch" data-src="https://www.labri.fr/perso/boussica/archives/cours/licence3/automates_grammaires/td1_automate_l3.pdf"></iframe>

<small style="font-size:10pt;float:right;">Source : TD de A. Boussicault, Univ. Bordeaux ([Solutions](https://www.labri.fr/perso/boussica/archives/cours/licence3/automates_grammaires/td1_corrige_automate_l3.pdf))</small>



## En pratique

- De nombreuses implantations (interpréteurs / compilateurs) de langages généralistes (dont Python) incluent nativement un support pour les expressions régulières.

- Pour Python, ce support est inclut dans la bibliothèque **re** [[&#x2197;](https://docs.python.org/fr/3/howto/regex.html)].

- Ces implantations utilisent parfois des sucres syntaxiques (syntaxe légèrement différente de celle présentée ici), pour **re** voir la brève introduction sur *melsophia.org* [[&#x2197;](http://python.melsophia.org/aspects/regex.html#syntaxe)].

- Pour tester une expression régulière Python : [pythex.org](https://pythex.org/), mémento des expressions régulières en python : [debuggex.com](https://www.debuggex.com/cheatsheet/regex/python)

- Pour aller plus loin, voir le chapitre 16 page 110 du cours de python de l'UFR de Sciences du vivant de Paris 7 [[&#x2197;](https://python.sdv.univ-paris-diderot.fr/cours-python.pdf)].



## Exercices (2/3)
<br>
- A partir du fichier ```insee.csv``` [[lien]](code/insee.csv), écrivez un script python permettant d'extraire les codes postaux des villes dont le nom :

	1. commence par ```a``` et dont la deuxième lettre est ```s``` ou ```t``` ;
	
	2. contient ```n``` et se termine par ```t``` ;
	
	3. contient ```gre``` ou ```st``` ;

	4. contient au moins deux ```m``` ; exactement deux ```m``` ;

	5. contient au moins quatre caractères consécutifs et aucun chiffre ;
	
	6. est constitué de deux lettres exactement.

<small style="font-size:10pt;float:right;">([Solutions](code/regexp.py))</small>



## Exercices (3/3)
<br>
- A partir du fichier ```poeme.txt``` [[lien]](code/poeme.txt), écrivez un script python permettant d'extraire les lignes :

 1. contenant le mot ```je```
 
 2. contenant le mot ```Vous``` en ignorant la casse

 3. ne contenant pas le mot ```Que``` en ignorant la casse
 
 4. commençant par ```Qu```
 
 5. se terminant par ```asse```
 
 6. contenant le mot ```vous``` et pas le mot ```je```



## <br><br><br><br><br><br>Langages hors-contexte
---



## Grammaires hors contexte (rappels)
<br>
- Définition :
<center>
$G$ = ($V_T, V_N, S, P$)
<br>
$P \subseteq V_N^+ \times (V_T \cup V_N)^*$
</center>

- Langage engendré par une grammaire hors-contexte G :
<center>
$L(G)$ = {$w \in V_T^∗ \mid\ S \Rightarrow_G^* w$}
</center>

- Exemple : 
$G$ = ($V_T, V_N, E, P$) où 

  - $V_T$= {$∗,+,−,tok,(,)$}, 
  - $V_N$ = {$E,T,F$},
  - $S$ = $E$,
  - $P=${$E \rightarrow E + T$, $E \rightarrow E − T$, $E \rightarrow  T$, $T \rightarrow T ∗ F$, $T \rightarrow  F$, $F \rightarrow tok$, $F \rightarrow (E)$}



## Grammaires hors contexte (suite)
<br>
- Les langages hors contexte sont *clos* pour l'union, la concaténation, l'étoile de Kleene, mais pas pour le complémentaire, l'intersection et le miroir

- Soit $e$ une expression arithmétique

- pour vérifier si $e$ appartient au langage $L(G)$ (noté $e \in L(G)$) on cherche une dérivation à partir de l'axiome $E$ qui génère $e$ (par réécriture) :

  - Soient $\gamma_1, \gamma_2, ..., \gamma_n \in (V_T \cup V_N)^*$
  
  - $E \Rightarrow_G \gamma_1 \Rightarrow_G \gamma_2 \Rightarrow_G ... \Rightarrow_G \gamma_n \Rightarrow_G e$

  - noté également $E \Rightarrow_G^* e$



## Grammaires hors contexte (suite)
<br>
- Exemple : (avec rappel de $P$ en factorisant les règles)<br><br>
$$E \rightarrow E + T \mid\ E − T \mid\ T$$
$$T \rightarrow T ∗ F \mid\ F$$
$$F \rightarrow tok \mid\ (E)$$

- Soit l'expression $a + b ∗ 3$

- Recherche d'une dérivation :

$$E \Rightarrow E + T \Rightarrow E + T ∗ F \Rightarrow T + T * F $$
$$\Rightarrow F + T ∗ F \Rightarrow a + T ∗ F \Rightarrow a + T ∗ 3 $$
$$\Rightarrow a + F ∗ 3 \Rightarrow a + b ∗ 3$$

- Donc $a + b ∗ 3 \in L(G)$



## Grammaires hors-contexte (suite)
<br>
- **A noter** : la dérivation peut ne pas être unique (en cas d'ambiguïté grammaticale)

- Représentation graphique d'une dérivation (arbre syntaxique) :

<p style="width:65%;float:left;"> 
<small>
$$E \Rightarrow E + T \Rightarrow E + T ∗ F \Rightarrow T + T * F $$
$$\Rightarrow F + T ∗ F \Rightarrow a + T ∗ F \Rightarrow a + T ∗ 3 $$
$$\Rightarrow a + F ∗ 3 \Rightarrow a + b ∗ 3$$
</small>
</p>

<p style="float:right;width:35%;">
![](images/ast.png) <!-- .element: class="plain" -->
</p>
<br><br><br><br>
- S'il y a ambiguïté grammaticale pour l'expression à analyser, on obtient une forêt d'analyse (graphe combinant plusieurs arbres)



## Algorithme(s) de reconnaissance (et d'analyse)
<br>
- A partir d'une expression $e$ :  

  - déterminer si $e$ appartient au langage $L(G)$ (**reconnaissance**)
  
  - construire le (ou les) arbre(s) syntaxique(s) décrivant la *structure* de $e$ (**analyse syntaxique**)
  
- Différents algorithmes d'analyse :

  - ascendant (on part des symboles terminaux constituant $e$)
  
  - descendant (on part de l'axiome)

<small>NB: certains algorithmes fonctionnent avec un sous-ensemble de $L^2$ (e.g., algorithme descendant LR en temps linéaire pour les grammaires hors-contexte déterministes)</small>



## Algorithme de reconnaissance : exemple de CYK
<br>
- Algorithme de **Cocke - Younger - Kasami** (1970)

- Algorithme ascendant reposant sur une table d'analyse (*chart*)

- Exemple :

![](images/grammaire_fourchette.png) <!-- .element: class="plain" style="width:20%;float:left;" -->

![](images/analyse_fourchette.png) <!-- .element: class="plain" style="width:70%;float:right;" -->



## Algorithme CYK en pseudo-code
<br>
<p style="font-size:22pt">
Pour $i = 1$ à $|m|$
<br>
&nbsp;&nbsp;&nbsp;&nbsp;  $P[i,i]$ := ensemble des non-terminaux $N$ tel que $N\rightarrow m[i] \in P$ 
<br>
Pour $d = 1$ à $|m|-1$
<br>
&nbsp;&nbsp;&nbsp;&nbsp; Pour $i = 1$ à $|m|-d$
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $j$ := $i+d$
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $P[i,j]$ := ensemble vide
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pour tout $k = i$ à $j-1$
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pour tout $B$ dans $P[i,k]$ et $C$ dans $P[k+1,j]$
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Pour tout non-terminal $N$ tel que $N\rightarrow BC$ est une règle
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ajouter $N$ à $P[i,j]$
<br>
Retourne oui si $S$ est dans $P[1,|m|]$ ; non sinon
</p>



## Algorithme CYK (suite)
<br>
- La grammaire doit être sous forme normale de Chomsky (CNF), c'est-à-dire que les règles sont de la forme 

  - $X \rightarrow Y Z$ (avec $X, Y, Z \in V_N$) ou 
  
  - $X \rightarrow a$ (avec $a \in V_T$)

- NB: Toute grammaire hors-contexte peut être transformée en grammaire hors-contexte sous forme normale de Chomsky équivalente

- L'algorithme CYK est en $O(n^3 \times |G|)$ avec $n$ la longueur de la chaîne à reconnaître / analyser, et $|G|$ le nombre de symboles dans les règles de $|G|$.



## Passage sour forme normale de Chomsky

-  Pour chaque $a \in V_T$, choisir $A \in V_N$ tel que $A \rightarrow a \in P$ (s'il n'existe pas de tel $A$, le créer et ajouter la règle). 

- Remplacer toutes les occurrences de $a$ dans les autres règles par $A$.

- Puis, pour toute règle $R$ de la forme $A \rightarrow B_1 B_2 ... B_n \in P$, on crée $B_2', ... , B_n'$<sub><span style="font-size:12pt">−</span></sub>$_1 \in V_N$, et on remplace $R$ par les règles 

  - $A \rightarrow B_1 B_2'$, 
  - $B_2' \rightarrow B_2 B_3'$, 
  - ... , 
  - $B_n'$<sub><span style="font-size=12pt">-</span></sub>$_1 \rightarrow B_n$<sub><span style="font-size:12pt">-</span></sub>$_1 B_n$

La complexité de cette opération est en $O(|G|)$ où $|G|$ est le nombre de symboles dans les règles de $G$.

<small style="float:right;">Source : [J. Peignier](http://perso.eleves.ens-rennes.fr/people/joshua.peignier/CYK.pdf)</small>



## Algorithme CYK en python
<br>
<pre><code data-sample='https://raw.githubusercontent.com/Naereen/notebooks/master/agreg/Algorithme%20de%20Cocke-Kasami-Younger%20(python3).py#77-78,86,88,90,110-112,114,242-243,433-468'></code></pre>

<small style="float:right;">Source: [L. Besson](https://nbviewer.jupyter.org/github/Naereen/notebooks/blob/master/agreg/Algorithme%20de%20Cocke-Kasami-Younger%20%28python3%29.ipynb#2.-Algorithme-de-Cocke-Kasami-Younger)</small>



## Algorithme CYK : exemple
<br>
<pre style="width:55%;float:left;"><code data-sample='https://raw.githubusercontent.com/Naereen/notebooks/master/agreg/Algorithme%20de%20Cocke-Kasami-Younger%20(python3).py#204,206,208,218-219,221-225,228-233,236,238-240'></code></pre>

![](https://upload.wikimedia.org/wikipedia/commons/f/f5/CYK_algorithm_animation_showing_every_step_of_a_sentence_parsing.gif) <!-- .element: class="plain" style="width:40%;float:right;" -->



## Exercices

<iframe class="stretch" data-src="https://www.linguist.univ-paris-diderot.fr/~amsili/Ens16/pdf/tb/TD2_RationnelsETAlgebriques.pdf"></iframe>

<small style="font-size:10pt;float:right;">Source : P. Amsili / T. Bernard, Univ. Paris 7 ([Solutions](http://utilisateurs.linguist.univ-paris-diderot.fr/~amsili/Ens16/pdf/tb/TD2_RationnelsETAlgebriques.corrected.pdf))</small>



## <br><br><br><br><br><br> Compilation
---



## Grammaire hors contexte et compilation

- *langages informatiques* (de programmation ou de description) sont généralement **définis par des grammaires hors contexte**

- *Les (!) analyseurs syntaxiques* (selon l'algorithme utilisé) sont **générés** (compilés) **automatiquement** à partir de la grammaire (éventuellement sous une forme imposée)

- Exemples :

|Compilateur d'analyseur||Algo. d'analyse|
|:--|---|:--|
|[Yacc](http://dinosaur.compilertools.net/) / [GNU Bison](https://www.gnu.org/software/bison/) (écrits en C)||Ascendant (LALR)|
|[ANTLR](https://www.antlr.org/) (Java)||Descendant (LL)|
|[JavaCC](https://javacc.org/) (Java)||Descendant (LL)|
|[Lark-parser](https://github.com/lark-parser/lark) (Python)||LALR / Earley / CYK|



## Lark-parser

- S'installe directement via ```pip3 install lark-parser```

- Intègre les compilateurs de l'analyseur lexical et syntaxique

- Prend en entrée des grammaires sous Forme de Backus-Naur Etendue (EBNF) [[Norme ISO-14977](https://www.iso.org/standard/26153.html)]
  - ```A = B C```
  - ```A = B | C```
  - ```A = [ B ]```
  - ```A = { B }```

- Exemple : (voir aussi l'[aide mémoire Lark](https://github.com/lark-parser/lark/blob/master/docs/_static/lark_cheatsheet.pdf) [[&#x2197;](https://github.com/lark-parser/lark/blob/master/docs/lark_cheatsheet.pdf)])

<pre class="stretch"><code data-sample='https://raw.githubusercontent.com/lark-parser/lark/master/examples/fruitflies.py#9-21'></code></pre>



## Lark-parser (suite)

- Contient des analyseurs lexicaux pré-définis :

<pre class="stretch"><code data-sample='https://raw.githubusercontent.com/lark-parser/lark/master/lark/grammars/common.lark#2-3,5-11,30-31,32-39,42-43,45'></code></pre>



## Lark-parser (suite)

- Utilisation :

  1. Définition de la grammaire EBNF (cf pages précédentes)

  2. Génération de l'analyseur syntaxique :<p>```from lark import Lark```<br>```p = Lark(grammar, start='sentence', parser='cyk', lexer='standard')```</p>

  3. Analyse d'un énoncé :<p>```p.parse(s)```</p>
  
- Résultat : arbre de syntaxe abstraite (AST)

<small>```Tree(_ambig, [Tree(sentence, [Tree(noun, [Token(NOUN, 'fruit')]), Tree(verb, [Token(VERB, 'flies')]), Tree(noun, [Token(NOUN, 'bananas')])]), Tree(sentence, [Tree(noun, [Tree(adj, [Token(ADJ, 'fruit')]), Token(NOUN, 'flies')]), Tree(verb, [Token(VERB, 'like')]), Tree(noun, [Token(NOUN, 'bananas')])])])```</small>



## Lark-parser (suite)
<br>
Analyse de l'énoncé (syntaxiquement ambigu) : 

<center>
```fruit flies like banana```
</center>
<br>

<pre style="width:41%;float:left;font-size:10pt">
<code>Tree(_ambig, 
   [Tree(sentence, 
      [Tree(noun, 
	      [Token(NOUN, 'fruit')]), 
	   Tree(verb, 
	      [Token(VERB, 'flies')]), 
	   Tree(noun, 
	      [Token(NOUN, 'bananas')])]), 
	Tree(sentence, 
	  [Tree(noun, 
	      [Tree(adj, 
		     [Token(ADJ, 'fruit')]), 
		   Token(NOUN, 'flies')]), 
	   Tree(verb, [Token(VERB, 'like')]), 
	   Tree(noun, [Token(NOUN, 'bananas')])])])
</code></pre>

![](code/figure.png) <!-- .element: class="plain" style="width:58%;float:right;" -->



## Architecture d'un compilateur <span style="font-size:12pt;float:right;">(Source : [wikipedia](https://fr.wikipedia.org/wiki/Compilateur))</span>
<center>
![](https://upload.wikimedia.org/wikipedia/commons/e/e5/Cha%C3%AEne_de_compilation.svg) <!-- .element: class="plain" style="width:72%" -->
</center>



## Analyse sémantique
<br>
- Grammaire :

<pre><code data-sample='https://raw.githubusercontent.com/lark-parser/lark/master/examples/fruitflies.py#13-14'></code></pre>

- Arbre de syntaxe abstraite *décoré* :

<pre style="width:42%;float:left;font-size:10pt">
<code>Tree(_ambig, 
   [Tree(comparative, 
      [Tree(noun, 
	      [Token(NOUN, 'fruit')]), 
	   Tree(verb, 
	      [Token(VERB, 'flies')]), 
		   Tree(noun, 
		      [Token(NOUN, 'bananas')])]), 
	Tree(simple, 
	   [Tree(noun, 
	      [Tree(adj, 
		     [Token(ADJ, 'fruit')]), 
		   Token(NOUN, 'flies')]), 
	    Tree(verb, 
		     [Token(VERB, 'like')]), 
	    Tree(noun, [Token(NOUN, 'bananas')])])])
</code></pre>

![](code/figure2.png) <!-- .element: class="plain" style="width:57%;float:right;" -->



## Génération de code
<br>
- Par parcours de l'arbre de syntaxe abstraite décoré 

- Dans ```lark-parser```, utilisation de la classe ```transformer```

- Exemple : considérons la grammaire du format JSON suivante 

<pre class="stretch"><code data-sample='https://raw.githubusercontent.com/lark-parser/lark/master/docs/json_tutorial.md#97-111'></code></pre>



## Génération de code (suite)

- Considérons l'énoncé <span style="font-size:16pt">```text = {"key": ["item0", "item1", 3.14, true]}```</span>

- Arbre de syntaxe abstraite produit par <span style="font-size:16pt">```json_parser.parse(text)```</span> :

<pre style="font-size:10pt">
<code>Tree(dict, 
   [Tree(pair, 
       [Tree(string, 
	       [Token(ESCAPED_STRING, '"key"')]), 
	    Tree(list, 
		   [Tree(string, 
		       [Token(ESCAPED_STRING, '"item0"')]), 
		    Tree(string, 
			   [Token(ESCAPED_STRING, '"item1"')]), 
		    Tree(number, [Token(SIGNED_NUMBER, '3.14')]), 
			Tree(true, [])])])])
</code></pre>

<center>
![](code/aout.png) <!-- .element: class="plain" style="width:85%;" -->
</center>



## Génération de code (suite)
<br>
- Parcours /transformation de l'arbre syntaxique abstrait :

<pre><code class="python">class MyTransformer(Transformer):
    def list(self, items):
        return list(items)
    def pair(self, apair):
        return apair
    def dict(self, items):
        return dict(items)
</code></pre>

- Code produit après appel de <span style="font-size:18pt">```MyTransformer().transform(ast)```</span> :

<pre style="font-size:10pt">
<code>{Tree(string, [Token(ESCAPED_STRING, '"key"')]): 
     [Tree(string, [Token(ESCAPED_STRING, '"item0"')]), 
	  Tree(string, [Token(ESCAPED_STRING, '"item1"')]), 
	  Tree(number, [Token(SIGNED_NUMBER, '3.14')]), 
	  Tree(true, [])]}
</code></pre>



## Génération de code (fin)

- Cas d'un générateur produisant du JSON en sortie:

<pre><code class="python">class TreeToJson(Transformer):
    def string(self, s):
        return s[0][1:-1]
    def number(self, n):
        return float(n[0])

    list = list
    pair = tuple
    dict = dict

    null = lambda self, _: None
    true = lambda self, _: True
    false = lambda self, _: False
</code></pre>

- Code produit après appel de <span style="font-size:18pt">```TreeToJson().transform(ast)```</span> :

<pre style="font-size:10pt">
<code>{'key': ['item0', 'item1', 3.14, True]}
</code></pre>

- Le code produit est finalement *exécuté* par une machine virtuelle (éventuellement interne) ou une machine physique (i.e., un processeur doté d'un langage machine spécifique)



## Compilation (fin)

- Quelques exemples de compilation / traduction de langages :

  - $\LaTeX$ &nbsp;&nbsp; vers PDF
  
  - C vers Assembleur
  
  - ...
  
  - Java vers Bytecode (vers Assembleur)

<center>
![](https://www.researchgate.net/profile/Michael_Orlov3/publication/318376879/figure/fig1/AS:544876321751040@1506920046971/Java-source-code-is-first-compiled-to-bytecode-and-subsequently-interpreted-or-executed.png) <!-- .element: class="plain" style="width:60%" -->
</center>



## Exercice

<iframe class="stretch" data-src="code/tp_lark.pdf"></iframe>

<small style="font-size:10pt;float:right;">Solutions: [Ex.1.1](code/calculatrice.py), [Ex.2](code/calculatrice2.py), [Ex.3](code/calculatrice3.py)</small>

Notes: https://lark-parser.readthedocs.io/en/latest/lark_cheatsheet.pdf



## Références

- *Compilation*. Notes de cours de Fabienne Carrier et Catherine Parent-Vigouroux, Université de Grenoble Alpes [[&#x2197;](http://www-verimag.imag.fr/~carrier/ISN-LangageCompilation/)]

- *Introduction à la programmation*. Notes de cours de Jean Baptiste et Luc Maranget, INRIA Paris [[&#x2197;](http://gallium.inria.fr/~maranget/X/421/poly/index.html)]

- *Théorie des langages*. Notes de cours de François Yvon et Akim Demaille, Université Paris Sud [[&#x2197;](https://perso.limsi.fr/yvon/classes/thl/thl-2.pdf)]

- *Langages formels, Calculabilité et Complexité*. Notes de cours d'Olivier Carton, ENS Ulm [[&#x2197;](https://gaati.org/bisson/tea/lfcc.pdf)]

- *Automates à piles*. Notes de cours de Thomas Blossier, Université Lyon 1 [[&#x2197;](http://math.univ-lyon1.fr/~blossier/odi2009/chap8.pdf)]

- Cours python en ligne sur le site developers.google [[&#x2197;](https://developers.google.com/edu/python/regular-expressions)]



# <br><br><br><br><br>Merci pour votre attention
