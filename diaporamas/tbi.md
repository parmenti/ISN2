 <span class="menu-title" style="display: none">Titre</span>
![ESPE](images/logo_ul_espe_fond_transparent300dpi.png)<!-- .element: class="plain" style="width:60%;float:left;"--> 

<br><br><br><br><br><br>

---

**<br>Le tableau blanc interactif : dispositifs et usages <br>**

<br><br>_Master MEEF 1er degré - Année 2018 / 2019_<br><br><br>Yannick Parmentier, ESPE de Lorraine<br>



# <br><br><br><br>Le tableau blanc interactif
<br>
&Eacute;tat des lieux

Notes: Définition, description, caractéristiques, fonctionnalités



## Le TBI : un outil révolutionnaire ?
<center>
![](http://vidberg.blog.lemonde.fr/files/2009/06/news29.1245401757.gif)<!-- .element: class="plain" style="width:55%;" -->
<center>
<span style="font-size:12px;float:right;">Source: [Blog « l'actualité en patates» par Martin Vidberg](http://vidberg.blog.lemonde.fr/2009/06/19/fantasmes-denseignants/)</span>



## Le tableau blanc interactif (TBI)
<br>
Définition :
<br>
<quote class="fragment" data-fragment-index="2">Le Tableau Blanc Interactif (appelé parfois Tableau Numérique Interactif) est un <b>dispositif numérique</b> pouvant être relié à un ordinateur afin d'étendre ses possibilités d'<b>interaction</b>.</quote>
<br><br>
<span class="fragment" data-fragment-index="3">En d'autres termes :</span>
<br><br>
<quote class="fragment" data-fragment-index="3">
- les <b>entrées</b> (informations saisies) ne proviennent pas uniquement du clavier ou de la souris &#x2192; <b>stylet</b> (ou doigt)
<br><br>
- la <b>sortie</b> (information affichée) est projetée sur une surface plus grande qu'un écran &#x2192; <b>tableau électronique</b> (ou veleda)
<br><br>
<span class="fragment" data-fragment-index="4">Invention aux USA en 1988, dans les écoles françaises depuis 2004</span>
</quote>



## Le(s) tableau(x) blanc(s) interactif(s) 
<br>
- Différents **types** de dispositifs :

  - **fixe** ou *mobile* (Dispositif Mobile Interactif)
  
  - **avec vidéo-projecteur** ou *non* (Ecran Tactile Interactif)
  
  - **avec tableau électronique** ou *non* (Vidéo Proj. Interactif)
  
  - **avec stylet** ou *non* 
<br>
- Différents constructeurs (liste non exhaustive) : <!-- .element: class="fragment" data-fragment-index="2" -->
  - EPSON (VPI) <!-- .element: class="fragment" data-fragment-index="2" -->
  
  - SMART, Promethean, Interwrite (TBI) <!-- .element: class="fragment" data-fragment-index="2" -->
  
  - Hitachi, Mimio (DMI) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#x25FE; Easypitch, BenQ (ETI) <!-- .element: class="fragment" data-fragment-index="2" -->



## Le tableau blanc interactif (suite)
<br>
Schéma de branchement (TBI classique) :<br><br>
<center>
![](images/tbi.png)<!-- .element: class="plain" style="width:80%;" -->
</center>
<br>
<p class="fragment" data-fragment-index="2">NB: Si l'on dispose d'un VPI, le tableau est remplacé par un simple support veleda *sur lequel on peut écrire avec un feutre*.</p>



## Le tableau blanc interactif (suite)
<br>
**Logiciel** de *pilotage* du dispositif ?
<br><br>
<quote class="fragment" data-fragment-index="2">

| Matériel   |   | Logiciel               |   | Système(s) |
|:-----------|:-:|:-----------------------|:-:|:----------:|
| EPSON      |   | [Easy Interactive Tools](https://www.epson.fr/epson-projector-software) |   | ![](images/Windows10_logo.png)<!-- .element: class="plain" style="width:100px;" --> ![](images/iOS_logo.svg)<!-- .element: class="plain" style="width:30px;" --> ![](images/Android_logo.svg)<!-- .element: class="plain" style="width:25px;" --> ![](images/Chromebook_logo.png)<!-- .element: class="plain" style="width:110px;" -->        |
| Interwrite |   | [Workspace](https://www.turningtechnologies.com/downloads/workspace/) |   | ![](images/Windows10_logo.png)<!-- .element: class="plain" style="width:100px;" --> ![](images/MacOS_logo.png)<!-- .element: class="plain" style="width:40px;" --> ![](images/iOS_logo.svg)<!-- .element: class="plain" style="width:30px;" --> ![](images/Android_logo.svg)<!-- .element: class="plain" style="width:25px;" --> ![](images/linux_logo.png)<!-- .element: class="plain" style="width:30px;" --> |
| Promethean |   | [ActivInspire](https://www1.support.prometheanworld.com/fr/download/activinspire.html)           |   | ![](images/Windows10_logo.png)<!-- .element: class="plain" style="width:100px;" --> ![](images/MacOS_logo.png)<!-- .element: class="plain" style="width:40px;" --> ![](images/linux_logo.png)<!-- .element: class="plain" style="width:30px;" --> |
| SMART      |   | [Notebook](https://education.smarttech.com/fr-fr/products/notebook/download)   &nbsp;[&#x20AC;]    |   | ![](images/Windows10_logo.png)<!-- .element: class="plain" style="width:100px;" --> ![](images/MacOS_logo.png)<!-- .element: class="plain" style="width:40px;" --> ![](images/iOS_logo.svg)<!-- .element: class="plain" style="width:30px;" --> ![](images/Android_logo.svg)<!-- .element: class="plain" style="width:25px;" -->  |

</quote>
<small>NB: certains dispositifs disposent d'un système embarqué (pas besoin de PC pour utiliser le TBI/VPI), d'autres encore peuvent être contrôlés par des tablettes.</small>



## Le tableau blanc interactif (suite)
<br>
- Alternative aux logiciels *propriétaires* des constructeurs : <br><br>
<center>
[Openboard](https://www.openboard.ch) (suite d'OpenSankoré-2.0)
<span style="font-size:12px;float:right;">[![](images/openboard_logo.png)<!-- .element: class="plain" -->](http://openboard.ch/)</span>
</center>
<br><br>
- Caractéristiques :
  - ouvert, gratuit, intuitif, multi-plateforme ![](images/Windows10_logo.png)<!-- .element: class="plain" style="width:100px;" --> ![](images/MacOS_logo.png)<!-- .element: class="plain" style="width:40px;" --> ![](images/linux_logo.png)<!-- .element: class="plain" style="width:30px;" -->

  - compatible avec la plupart des matériels 

  - documentation disponible dans plusieurs langues [[&#x2B67;](http://openboard.ch/support.html)]
  
  - maintenu activement (soutien de la Dir. Inst. Pub. de Genève)



# <br><br><br><br>Le logiciel Openboard
<br>
Fonctionnalités de base

Notes: boîte à outil pour interagir avec la classe



## Le logiciel Openboard
<br>
- **Pourquoi** utiliser un logiciel de pilotage de TBI ?

  - afficher des documents multimédia (textes, sons, images, vidéos, pages web, jeux, etc.) <!-- .element: class="fragment" data-fragment-index="2" -->
  
  - annoter des documents manuellement (via le stylet) <!-- .element: class="fragment" data-fragment-index="3" -->
  
  - enregistrer ses annotations (pour les reprendre plus tard) <!-- .element: class="fragment" data-fragment-index="4" -->
<br>
- Concrètement : <!-- .element: class="fragment" data-fragment-index="5"-->

  - fichiers affichés (.pdf, .mp3, etc.) enregistrés dans la <br>« bibliothèque » Openboard (répertoire local) <!-- .element: class="fragment" data-fragment-index="5" -->
  
  - fichiers annotés sauvegardés sous forme de « documents » composés de « pages » et enregistrés localement (.ubz) <!-- .element: class="fragment" data-fragment-index="5" -->
</quote>



## Le logiciel Openboard (suite)
<br>
- 4 modes disponibles : 

 1. **Mode tableau** (*espace vierge* pour y écrire, y insérer des images à partir de la bibliothèques, des applications, etc.)
 
 2. **Mode documents** (*explorateur de fichiers* permettant de visualiser les pages composant les documents enregistrés dans l'espace de travail) 

 3. **Mode bureau** (*boîte à outils* permettant d'annoter les informations du bureau, comme un logiciel en cours d'exécution, un fond d'écran, etc.)
 
 4. **Mode web** (*navigateur* internet intégré)



<section data-background-iframe="images/OpenBoard1.5_interface.pdf" data-background-interactive><span style="color:white;"><b>Mode tableau</b></span>
</section>



<section data-background-iframe="images/OpenBoard1.5_Mode_Doc_en_1_page.pdf" data-background-interactive><span style="color:white;"><b>Mode documents</b></span>
</section>



## Exercices
<br>
Afin de vous familiariser avec Openboard, réalisez en binôme, sur les PC de la salle, les exercices suivants : 
<br><br>
- Exercice 1 : annotation de fichier PDF [[&#x2B67;](https://edu.ge.ch/site/openboard/wp-content/uploads/sites/139/2016/12/activit%C3%A9s_1-1.odt)]

- Exercice 2 : création de document UBZ [[&#x2B67;](https://edu.ge.ch/site/openboard/wp-content/uploads/sites/139/2016/12/activit%C3%A9s_2.odt)]

- Exercice 3 : utilisation de ressources interactives <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(vidéos, cartes, etc.) [[&#x2B67;](https://edu.ge.ch/site/openboard/wp-content/uploads/sites/139/2016/12/activit%C3%A9s_3.odt)]
<br><br>
- Références : 

  - Site du Service &Eacute;cole-Médias (SEM) du Canton de Genève [[&#x2B67;](https://edu.ge.ch/site/openboard/formation-continue-cours-de-base/)]

  - Cours du SEM ouverts en ligne [[&#x2B67;](https://edu.ge.ch/cool)] 



# <br><br><br><br>Le tableau blanc interactif
<br>
Quelques usages « simples » en classe



## *Projection* (lecture) de documents
<br>
 - **productions d'élèves** (via un [visualiseur](https://fr.wikipedia.org/wiki/Visualiseur) ou une connexion USB / Wifi avec un smartphone / une tablette)
 
 - **fichiers divers** (via les logiciels de lecture associés : [LibreOffice](https://fr.libreoffice.org/), [Acrobat Reader](https://get.adobe.com/fr/reader/otherversions/), [VLC](http://www.videolan.org/), [XnViewMP](https://www.xnview.com/en/xnviewmp/), etc.)

 - **recherches internet** (via un moteur de recherche ou des pages webs pré-enregistrées dans les favoris) 
 
 - **cartes** géographiques (via les services en ligne [khartis](http://www.sciencespo.fr/cartographie/khartis/), [OpenStreetMap](https://umap.openstreetmap.fr/fr/), etc.)
 
 - **livres numériques** (via le logiciel [Calibre](https://calibre-ebook.com/fr) ou les plugins [EpubReader](https://addons.mozilla.org/fr/firefox/addon/epubreader/) pour Firefox ou [Readium](https://chrome.google.com/webstore/detail/readium/fepbnnnkkadjhjahcafoaglimekefifl) pour Chrome)

<small>NB: ces usages ne nécessitent pas de logiciel de pilotage de TBI.</small>



## *Création / annotation* (écriture) de documents 
<br>
 - **éditer** un document vierge via le pilote de TBI

 - **écrire** du texte ou **dessiner** des croquis via [autodraw.com](https://www.autodraw.com/)
 
 - **écrire** des formules ou **dessiner** des schémas via [MyScript.com](http://webdemo.myscript.com/#/home)

 - **dessiner** des oeuvres avec la plateforme [Sketch.io](https://sketch.io/sketchpad/)

 - **annoter** des pdf avec les logiciels [LibreOffice](https://dutailly.net/remplir-pdf-nest-pas-formulaire) ou [Xournal++](https://github.com/xournalpp/xournalpp)
 
 - **interagir** avec des applications dédiées : *exercices en ligne* sur [LearningApps.org](https://learningapps.org/), [MicEtF.fr](https://micetf.fr/index/) ou [AlloProf.qc.ca](http://www.alloprof.qc.ca/Pages/ResultatRechercheExercice.aspx), ou *logiciels* comme la suite [AbulEdu](https://www.abuledu.org/logiciels-pedagogiques-cycle1-cycle2-cycle3-abuledu/) (<small>voir liens dans la section « Logiciels pédagogiques » </small>)

<br><small>NB: les saisies se font à la **souris** ou au **stylet** (s'il est reconnu par le système, via Openboard ou autre).</small>



## Quelques ressources 
<br>
**documentaires** :

 - Livres de jeunesses en ligne (image + texte + son) [[&#x2B67;](http://seduc.csdecou.qc.ca/5-au-quotidien/ateliers-tic-ecouter-la-lecture/)]
 
 - Bibliothèque d'images libres de l'association AbulEdu [[&#x2B67;](http://data.abuledu.org/wp/)]
 
 - Portail éducatif de l'Institut Géographique National (IGN) [[&#x2B67;](http://education.ign.fr/)]
 
 - Géoportail [[&#x2B67;](https://www.geoportail.gouv.fr/)] &nbsp;&nbsp; &#x2022; Portail éducatif de Météo France [[&#x2B67;](http://education.meteofrance.fr/)] 
 
 - Eduthèque [[&#x2B67;](https://www.edutheque.fr/accueil.html)] (oeuvres culturelles) &nbsp;&nbsp; &#x2022; Manuels Sésamath [[&#x2B67;](https://manuel.sesamath.net/index.php?page=telechargement)]
 
et **méthodologiques** :

 - Numériser un document papier avec LibreOffice [[&#x2B67;](https://dutailly.net/scanner-avec-libreoffice)]

 - Rédiger un texte à trous avec LibreOffice [[&#x2B67;](https://dutailly.net/un-exercice-a-trous-avec-writer)]



# <br><br><br><br>Le tableau blanc interactif
<br>
Quelques usages « avancés » en classe



## Modeler les bonnes pratiques
<br>
**&Eacute;criture de textes :**

 - Vérifier l'orthographe d'un texte via *Scribens.fr* [[&#x2B67;](https://www.scribens.fr/#)]

 - Vérifier la prononciation d'un texte via *NaturalReaders.com* [[&#x2B67;](https://www.naturalreaders.com/online/)]
 
<small>NB: ces services utilisent la publicité, pensez à installer un [bloqueur de publicité](https://adblockplus.org/) dans votre navigateur.</small>
<br><br> 
**Recherche d'information :**

 - Faire connaître les moteurs de recherches adaptés (*Qwantjunior.com* [[&#x2B67;](https://www.qwantjunior.com/)], *Takatrouver.net* [[&#x2B67;](http://www.takatrouver.net/)], etc.)
 
 - Apprendre à rechercher des oeuvres réutilisables <br>(via *Creativecommons.org* [[&#x2B67;](https://search.creativecommons.org/)], etc.)

<small>NB: voir aussi le guide de la recherche sur internet de l'Université Laval [[&#x2B67;](http://www.faireunerecherche.fse.ulaval.ca/ressources/affiches/)].</small>



## Mobiliser la classe
<br>
- **Gérer le niveau sonore, les temps de travail, etc.** :

 - via l'outil *Classroomscreen.com* [[&#x2B67;](https://classroomscreen.com/)]

- **Présenter des applications interactives** :

 - via l'outil *LearningApps.org* [[&#x2B67;](https://learningapps.org/)]
 
 - via les applications de la « bibliothèque » d'Openboard

- **Créer des cartes mentales pour expliciter des raisonnements** :

 - via l'outil *Framindmap.org* [[&#x2B67;](https://framindmap.org/)]

- **Créer des questionnaires pour la classe** :

 - via l'outil *Plickers* [[&#x2B67;](http://mediafiches.ac-creteil.fr/spip.php?mot320)]



## Mobiliser la classe (suite)
<br>
- **Projeter des notes de cours (diaporamas)** :

 - Trucs et astuces du diaporama du *CRDP du Limousin* [[&#x2B67;](http://scenari.crdp-limousin.fr/TBI_introduction/co/trucsETastuces_global.html)]

 - Guide pratique du diaporama sur *Profweb.ca* [[&#x2B67;](http://www.profweb.ca/publications/dossiers/diaporamas-numeriques-en-classe)]

- **Intégrer le TBI dans des scénarios pédagogiques** :

 - exemples de fiches ressources créées par le pôle numérique de l'académie de Créteil pour le logiciel Openboard [[&#x2B67;](http://mediafiches.ac-creteil.fr/spip.php?rubrique34)] ou les autres logiciels de TBI [[&#x2B67;](http://mediafiches.ac-creteil.fr/spip.php?rubrique1)]
 
 - fiches ressources de l'Université Laval [[&#x2B67;](https://coa-tni.tact.fse.ulaval.ca/)] sur l'utilisation du TBI comme espace collectif pour une communauté d'apprentissage



## Exercice
<br>
- &Agrave; faire par binôme :

  - Choisir une fiche (tutoriel) TNI conçue par le pôle informatique de l'académie de Créteil [[&#x2B67;](http://mediafiches.ac-creteil.fr/spip.php?rubrique34)].
  
  - Sur un PC de la salle équipé d'Openboard, réaliser l'activité présentée sur la fiche (en prenant des notes afin de pouvoir la présenter oralement aux autres).
  
  - Déposer vos notes sur l'espace dédié sur le cours sur ARCHE.



# <br><br><br><br>Le tableau blanc interactif
<br>
Quels impacts sur les apprentissages ?



## Avantages et inconvénients du TBI
<br>
**Avantages** : 

- *richesse des contenus* (multimédia)

- *engagement des élèves* (attention accrue)

- *enregistrement de traces* (mémoire, réinvestissements)

- *interactivité forte* (construction collective des savoirs favorisée)

**Inconvénients** :

- *surcharge cognitive* (outil riche qui pourrait bruiter l'apprentissage)

- *pièges* (encouragement d'un apprentissage frontal, ressources pas toujours pertinentes, effet gadget)



## Résultats du Rapport Karsenti (2016)
<br>
Usages du TBI
<p style="float:left;width:90%;">
<canvas style="float:right;" class="stretch" data-chart="pie">
, Logiciels multimédia, recherche internet, vidéos, notes de cours, sciences, corrections de texte, livres numériques
, 51.6,19.3,10.8,6.9,4.8,1.7,1.5
center, 0,0,0,0,0,0,0
</canvas>
&nbsp;<br><span style="font-size:12pt;float:right;">Source: [Rapport Karsenti](http://tbi.crifpe.ca/files/Faits-saillants.pdf)</span>
</p>



## Résultats du Rapport Karsenti (suite)
<br>
Avantages du TBI
<p style="float:left;width:50%;">
pour les enseignants
<canvas style="float:right;" class="stretch" data-chart="pie">
, accès internet, support visuel, capsules vidéo, motivation, enseignement varié, enseignement efficace, apprentissage facilité
, 23.5, 19.1, 12.2, 11.8, 9.3, 0, 9.1
center, 0,0,0,0,0,0,0
</canvas>
&nbsp;<br><span style="font-size:12pt;float:right;">Source: [Rapport Karsenti](http://tbi.crifpe.ca/files/Faits-saillants.pdf)</span>
</p>
<p style="float:right;width:50%;">
pour les élèves
<canvas style="float:right;" class="stretch" data-chart="pie">
, accès internet, support visuel, capsules vidéo, motivation, enseignement varié, enseignement efficace, apprentissage facilité
, 29.2, 18.8, 0, 11.6, 9.5, 6.3, 6.1
center, 0,0,0,0,0,0,0
</canvas>
&nbsp;<br><span style="font-size:12pt;float:right;">Source: [Rapport Karsenti](http://tbi.crifpe.ca/files/Faits-saillants.pdf)</span>
</p>



## Résultats du Rapport Karsenti (suite)
<br>
Inconvénients du TBI
<p style="float:left;width:50%;">
pour les enseignants
<canvas style="float:right;" class="stretch" data-chart="pie">
, problèmes techniques, chronophage, écran trop petit, gestion de classe, manque de maîtrise du TBI, perte de motivation
, 70.6, 17.3, 9.6, 1.4, 0, 0
center, 0,0,0,0,0,0
</canvas>
&nbsp;<br><span style="font-size:12pt;float:right;">Source: [Rapport Karsenti](http://tbi.crifpe.ca/files/Faits-saillants.pdf)</span>
</p>
<p style="float:right;width:50%;">
pour les élèves
<canvas style="float:right;" class="stretch" data-chart="pie">
, problèmes techniques, chronophage, écran trop petit, gestion de classe, manque de maîtrise du TBI, perte de motivation
, 33.5, 0, 25.4, 0, 19, 18.3
center, 0,0,0,0,0,0
</canvas>
&nbsp;<br><span style="font-size:12pt;float:right;">Source: [Rapport Karsenti](http://tbi.crifpe.ca/files/Faits-saillants.pdf)</span>
</p>



## Exercice 
<br>
- &Agrave; faire par binôme :

 - Choisir une vidéo du site Pragmatice parmi celles associées au TBI [[&#x2B67;](http://pragmatice.net/lesite/spip.php?page=videos-classe&mc-g2=8)].
 
 - La visionner et analyser la pertinence de l'usage fait du TBI au moyen de la grille d'évaluation [[&#x2B67;](https://www.reptic.qc.ca/grille/)] proposée par Barette et al. (2011) [[&#x2B67;](http://www.infiressources.ca/fer/depotdocuments/Grille_d_analyse_du_scenario_d_une_activite_pedagogique_misant_sur_les_TIC-BaretteCollectif-Vol_24-4.pdf)].

 - Enregistrer la grille au format PDF et la déposer sur l'espace dédié sur le cours sur ARCHE (en insérant vos noms dans le nom du fichier).



## Références
<br>
- Usages d'un TBI en classe (CRDP du Limousin) [[&#x2B67;](http://scenari.crdp-limousin.fr/TBI_introduction/co/contenus_intro_web_simple.html)]

- Ressources pour TNI sur le site theosept.fr [[&#x2B67;](https://theosept.fr/spip.php?article163)]

- Fiches TBI du pôle numérique de l'académie de Créteil [[&#x2B67;](http://mediafiches.ac-creteil.fr/spip.php?rubrique1)]

- Présentation « 26 usages pour votre TBI » de Julie Beaupré [[&#x2B67;](https://docs.google.com/presentation/d/1kZTNW3Dh748MnmbV_CPKNGQnu4IIF67Tx6zkwPI0jsw/edit#slide=id.g35f391192_00)]

- Rapport du Pr Thierry Karsenti (Université de Montréal) sur l'impact du TBI sur les apprentissages [[&#x2B67;](http://tbi.crifpe.ca)]
