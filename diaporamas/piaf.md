 <span class="menu-title" style="display: none">Projet PIAF</span>
![ESPE](images/logo_ul_espe_fond_transparent300dpi.png)<!-- .element: class="plain" style="width:60%;float:left;"--> 
![LORIA](images/logo_loria.png)<!-- .element: class="plain" style="width:12%;float:right;"--> 
<br><br><br><br><br><br>

---

<br><br>_Développer la Pensée Informatique et Algorithmique dans l'enseignement Fondamental_ (Projet Erasmus+ PIAF)
<br><br><br>
Yannick Parmentier, Université de Lorraine, ESPE, LORIA

Notes: relation numérique / informatique, controverse sur le numérique à l'école



# Introduction
<br>
- Caractéristiques du numérique :

  1. Une place grandissante dans notre vie<!-- .element: class="fragment" data-fragment-index="2" -->

Notes: comment s'en convaincre : données du "baromètre du numérique"



<section>
<h2>Usage du téléphone pour naviguer sur internet (par tranche d'âge)</h2><br>
 <canvas class="stretch" data-chart="bar">
, 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018
12-17 ans, 10.69767441860465, 9.523809523809524, 15.09433962264151, 19.63470319634703, 27.64976958525346, 49.029126213592235, 57.28155339805825, 69.15422885572139, 80.48780487804879, 83.4862385321101, 86.82926829268293, 84.5
18-24 ans, 12.0, 11.48936170212766, 26.666666666666668, 29.535864978902953, 46.42857142857143, 59.817351598173516, 77.9591836734694, 81.85840707964603, 90.3061224489796, 90.0, 94.97716894977168, 96.38009049773756
25-39 ans, 7.74526678141136, 8.097928436911488, 16.91449814126394, 20.5078125, 36.08445297504799, 43.57429718875502, 56.28997867803838, 65.51020408163265, 79.53586497890295, 80.63829787234043, 89.1156462585034, 88.20861678004536
40-59 ans, 3.816793893129771, 2.6206896551724137, 6.078147612156296, 6.419400855920114, 12.177650429799428, 24.162011173184357, 29.069767441860467, 36.29842180774749, 46.1212976022567, 52.75908479138627, 66.14509246088194, 64.39169139465875
60-69 ans,0.4273504273504274, 1.680672268907563, 1.4925373134328357, 2.127659574468085, 5.762711864406779, 7.380073800738007, 15.755627009646304, 20.0, 24.516129032258064, 23.58490566037736, 34.394904458598724, 32.0
70+ ans, 0.3448275862068966, 0.0, 0.34965034965034963, 0.35842293906810035, 1.7482517482517483, 2.7027027027027026, 3.7162162162162162, 4.318936877076411, 8.571428571428571, 10.695187165775401, 16.819571865443425, 17.98780487804878
</canvas>
<p style="float:right;font-size:14px;">Données : [baromètre du numérique](https://www.data.gouv.fr/fr/datasets/barometre-du-numerique/)</p>
<aside class="notes">
Notes: internet (donc numérique) s'invite de plus en plus dans notre vie
</aside>
</section>



<section>
<h2>Démarches administratives réalisées par internet (par tranche d'âge)</h2>
<br>
 <canvas class="stretch" data-chart="bar">
, 2009,2010,2011,2012,2013,2014,2015,2016,2017, 2018
12-17 ans, 6.60377358490566, 6.8493150684931505, 7.834101382488479, 11.165048543689322, 6.310679611650485, 5.472636815920398, 5.853658536585367, 10.091743119266056, 24.878048780487806, 16.5
18-24 ans, 48.44444444444444, 54.85232067510548, 63.83928571428571, 54.794520547945204, 64.89795918367346, 55.309734513274336, 62.244897959183675, 69.47368421052632, 81.7351598173516, 76.92307692307693
25-39 ans, 65.61338289962825, 69.140625, 70.44145873320538, 72.48995983935743, 77.18550106609808, 78.77551020408163, 79.11392405063292, 87.65957446808511, 88.88888888888889, 88.88888888888889
40-59 ans, 46.59913169319827, 51.497860199714694, 55.44412607449857, 57.960893854748605, 61.04651162790697, 59.11047345767575, 63.32863187588153, 72.94751009421265, 78.66287339971551, 76.26112759643917
60-69 ans, 30.970149253731343, 29.78723404255319, 36.94915254237288, 30.627306273062732, 41.80064308681672, 49.50819672131148, 48.70967741935484, 54.40251572327044, 57.64331210191082, 55.14285714285714
70+ ans, 5.944055944055944, 8.60215053763441, 8.741258741258742, 12.162162162162163, 13.175675675675674, 16.943521594684384, 20.0, 27.540106951871657, 31.804281345565748, 32.6219512195122
</canvas>
<p style="float:right;font-size:14px;">Données : [baromètre du numérique](https://www.data.gouv.fr/fr/datasets/barometre-du-numerique/)</p>
<aside class="notes">
Notes: intrusion volontaire ou subie
</aside>
</section>



<section>
<h2>Démarches administratives réalisées par internet (par niveau de diplôme)</h2>
<br>
 <canvas class="stretch" data-chart="bar">
, 2009,2010,2011,2012,2013,2014,2015,2016,2017,2018
sans diplôme, 14.15929203539823, 15.06172839506173, 17.892156862745097, 18.37270341207349, 21.867321867321866, 21.54696132596685, 19.018404907975462, 29.360465116279073, 30.48780487804878, 26.36986301369863
niveau Brevet, 38.89618922470434, 40.234375, 42.513368983957214, 43.77510040160642, 46.98972099853157, 47.646219686162624, 49.93234100135318, 58.45539280958722, 65.0, 63.04044630404463
niveau Bac, 55.19287833827893, 60.11080332409973, 68.6046511627907, 64.04833836858006, 66.66666666666666, 67.79661016949152, 72.95918367346938, 76.01010101010101, 82.7250608272506, 78.57142857142857
diplôme du supérieur, 73.7991266375546, 76.72955974842768, 77.09923664122137, 75.04621072088725, 82.09219858156028, 78.57142857142857, 80.98720292504571, 86.25827814569537, 89.25619834710744, 87.5195007800312
</canvas>
<p style="float:right;font-size:14px;">Données : [baromètre du numérique](https://www.data.gouv.fr/fr/datasets/barometre-du-numerique/)</p>
<aside class="notes">
Notes: pas tous égaux devant cette intrusion
</aside>
</section>



# Introduction (suite)
<br>
- Caractéristiques du numérique :

  1. Une place grandissante (et inégale) dans notre vie

  2. Des applications toujours plus nombreuses et<br> plus puissantes (complexité accrue)<!-- .element: class="fragment" data-fragment-index="2" -->

Notes: voyons quelques domaines d'application du numérique



<section tagcloud large> <span class="menu-title" style="display: none">Quelques applications du numérique</span>
<br><br>
    Finance
	Chirurgie
	Météorologie
	Traduction
	Recherche d'information
	Communication
	Information & média
	Jeux
	Transports
	Médecine
	Art
	Loisirs
	Education
	Ecologie
	Sécurité
	Energie
<aside class="notes">
Notes: véhicule autonome, opération à distance, jeu de go, etc.
</aside>
</section>



# Introduction (suite)
<br>
- Caractéristiques du numérique :

  1. Une place grandissante (et inégale) dans notre vie

  2. Des applications toujours plus nombreuses et<br> plus puissantes (complexité accrue)
  
  3. Des interfaces toujours plus simples<!-- .element: class="fragment" data-fragment-index="2" -->

Notes: paradoxe complexité des traitements / simplicité des interfaces



# &Eacute;volution des interfaces
<br>
![avant](https://upload.wikimedia.org/wikipedia/en/e/e2/Pcpaint_000.png)<!-- .element: style="width:35%;float:left;" -->
![apres](https://cdn.mos.cms.futurecdn.net/pjHhNJT7mASk5v6U3Xrt5Q-650-80.jpg)<!-- .element: style="width:35%;float:right;" -->

<br><br><br><br><br>

![avant2](https://upload.wikimedia.org/wikipedia/commons/7/74/THinkPad345C.jpg)<!-- .element: style="width:35%;float:left;" class="fragment" data-fragment-index="2" -->
![apres2](https://upload.wikimedia.org/wikipedia/commons/f/fe/Nexus_10.png)<!-- .element: style="width:35%;float:right;" class="fragment" data-fragment-index="2" -->

Notes: paradoxe s'applique tant au logiciel qu'au matériel



# Introduction (suite)
<br>
- Caractéristiques du numérique :

  1. Une place grandissante (et inégale) dans notre vie

  2. Des applications toujours plus nombreuses et<br> plus puissantes (complexité accrue)
  
  3. Des interfaces toujours plus simples
  
  4. **Un nouveau rapport au temps, à l'espace**<!-- .element: class="fragment" data-fragment-index="2" -->

Notes: info en temps réel / accessible partout, notion de véracité/confiance remise en cause



# Introduction (suite)
<br>
- Des questions se posent alors :

 - 0 Qu'est ce que le numérique ?<!-- .element: class="fragment" data-fragment-index="2" -->
<br><p class="fragment" data-fragment-index="2">**numérique** = outils logiciels / matériels + données (binaires) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ normes + *usages*</p>

 - 1 Doit on apprendre à <strike>utiliser</strike> appréhender le numérique ? 
 
 - 2 Est ce le rôle de l'&Eacute;cole d'initier cet apprentissage ?
 
 - 3 Comment faire ?

Notes: lien entre outils, données, normes et usages sous-estimé



# Appréhender le numérique
<br>
- Appréhender le numérique &#x21d2; Apprendre l'informatique ?<br><br>

- Il est important d'avoir de « bonnes » images<br><br><!-- .element: class="fragment" data-fragment-index="2" -->

- On peut commencer tôt à se construire ces images <br>(en particulier entre 5 et 12 ans) <!-- .element: class="fragment" data-fragment-index="3" -->

  <p class="fragment" data-fragment-index="3">- cela ne nécessite pas forcément de matériel informatique <br>&nbsp;&nbsp;&nbsp;ni une grande expérience de l'informatique</p>

  <p class="fragment" data-fragment-index="3">- projet PIAF : développer ces représentations / cette pensée</p>

Notes: question ouverte, contexte de dépendance numérique



# <br><br><br><br><br> Le projet PIAF

Notes: présentation générale, partenaires, sujet, méthodologie, objectifs précis (objectif généraux déjà vus)



<section data-background-iframe="https://framacarte.org/fr/map/piaf_44199" data-background-interactive>
</section>



# Le projet
<br>
<center>
<img class="stretch" src="images/summary-piaf.png"/>
</center>

Notes: action clé 1 -> mobilité à des fins d'apprentissage, action clé 2 -> coopération pour l'innovation et l'échange, section 1 -> enseignement scolaire, 2 -> enseignement supérieur, 3 -> formation professionnelle, 4 -> éducation des adultes, 6 -> sport



# Sujet

- Développer la _pensée informatique et algorithmique_

> Penser informatiquement, c'est utiliser l'abstraction et la
décomposition quand il s'agit d'affronter une tâche large et complexe
ou quand il s'agit de concevoir un système large et complexe ; c'est
séparer les différents aspects ; c'est choisir une représentation
appropriée pour un problème ou modéliser les aspects pertinents d'un
problème pour les rendre accessible (...)

<p style="float:right;font-size:12;">Jeannette Wing (2006)</p>

Notes: article paru dans le bulletin de l'ACM (plus large société scientifique au monde, créée en sept. 1947, délivre le Prix Turing)



# Méthodologie 
<br>
1. Conception d'un référentiel de compétences

2. Définition (et mise en oeuvre) de scénarios pédagogiques pour l'acquisition de ces compétences 

3. &Eacute;valuation de l'impact de ces scénarios sur des apprenants

Notes: 1. et 2. réalisés par ailleurs, 3. est nouveau



# Référentiel de compétences
<br>
- Etat des lieux : plusieurs référentiels existent dont notamment

   - [DigComp 2.1 (EU, 2013 révisé en 2017)](https://publications.europa.eu/fr/publication-detail/-/publication/3c5e7879-308f-11e7-9412-01aa75ed71a1) 
   
   - [CSTA Standards (USA, 2011, révisé en 2017)](https://drive.google.com/file/d/13QUISBtrOVfpbJcEJD8_7ibJqxuGvx1r/view)
   
   - [Digital Technologies Hub (AUS, 2016)](https://www.digitaltechnologieshub.edu.au/images/default-source/getting-started/dthub_infographic-web-1-1-resizedb032449809f96792a599ff0000f327dd.jpg)
   
   - [Teaching London Computing (UK, 2014)](https://teachinglondoncomputing.files.wordpress.com/2014/07/computing_progression_pathways_with_computational_thinking_v2-3.pdf)

- Limitations :

   - Distinction compétences utilisateur / pensée informatique
   
   - Progression inadaptée (curriculums "trop" informatique) 

Notes: Référentiels définis dans les pays développés, modèle européen "apprendre à nager dans un environnement numérique", support pour le futur "cadre de référence des compétences numériques à l'école et au collège"



<section>
<h1>Proposition de référentiel</h1>
<center>
<img scroll="yes" height="550px" src="images/CompetencesPIAmarie26_02.png"/>
</center>
</section>



# Réalisation de scénarios pédagogiques
<br>
- Plusieurs sources d'inspiration :

  - Livre [1,2,3 Codez](http://www.fondation-lamap.org/123codez) paru aux éditions Pommier (2016) 
  
  - Site [CS Unplugged](https://classic.csunplugged.org) présentant des activités débranchées rassemblées depuis 2005 (Nouvelle Zélande)
  
  - Page [Sciences Manuelles du Numériques](http://people.irisa.fr/Martin.Quinson/Mediation/SMN/)
  
  - Programmes [Coding Lab (Asie)](https://www.codinglab.com.sg/our-classes/)



# Séminaires communs

<div class="tweet" data-src="https://twitter.com/MarieKremer3/status/1102595815535771648"></div>



# Séminaires communs (suite)

<div class="tweet" data-src="https://twitter.com/MarieKremer3/status/1102570955656843264"></div>



# Conclusion
<br>
- approche transnationale visant à définir un cadre commun pour l'apprentissage de la pensée informatique et algorithmique 

- travaux en cours :  évaluation des apprentissages de la pensée informatique à construire

- compétences transverses (logique, mathématiques, langage, éducation civique, etc.) &#x2260; "simples" compétences d'utilisateur / programmeur informatique

- compatible avec une éducation à l'exposition aux écrans et à une sensibilisation aux conséquences environnementales de la révolution numérique



# <br><br><br><br><br> Merci pour votre attention
