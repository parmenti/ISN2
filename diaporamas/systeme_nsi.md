 <span class="menu-title" style="display: none">Titre</span>
![ESPE](images/logo_ul_espe_fond_transparent300dpi.png)<!-- .element: class="plain" style="width:60%;float:left;"--> 
![LORIA](images/logo_loria.png)<!-- .element: class="plain" style="width:12%;float:right;"--> 
<br><br><br><br><br><br>

---

**<br>Formation NSI - Bloc 3 - Système<br>**

<br><br>_Juillet 2019_<br><br><br>Yannick Parmentier



## Introduction

<p>**Système (d'exploitation)** : <br>
  un ensemble de programmes qui dirige l'utilisation des *ressources* d'un ordinateur<span style="font-size:12pt;float:right;"><br>(source: [wikipedia](https://fr.wikipedia.org/wiki/Langage))</span></p>

<p class="fragment" data-fragment-index="2">
**Ressources d'un ordinateur** : <br>
ressources *virtuelles* (fichiers, programmes en cours d'exécution) et *physiques* (processeur, mémoire, disque dur, périphériques) <br>
</p>
<p class="fragment" data-fragment-index="3">
**Rôle** : s'abstraire des contraintes matérielles, offrir un accès unifié aux ressources
</p>
<p class="fragment" data-fragment-index="4">
 &#x2192; Interface entre logiciel et matériel
 <br>
 &#x2192; Interface entre Homme et machine
</p>
<p class="fragment" data-fragment-index="5">
 &#x27A1; <b>Composant logiciel central</b> d'un ordinateur (qu'il s'agisse d'un PC, serveur, tablette, smartphone, système embarqué, etc.) 
</p>



## Plan
<br>
1. Histoire des ordinateurs en quelques dates

2. Principales fonctions d'un Système d'Exploitation (SE)

3. Principaux SE utilisés actuellement

4. Caractéristiques du SE Linux

5. Les composants (gestionnaires) d'un SE

6. La virtualisation

7. Interagir avec le SE : le Shell



## <br><br><br><br><br><br>Une brève Histoire des ordinateurs
---
<br> Exploitation du matériel des années 40 à nos jours



## Histoire des ordinateurs

<p style="float:left;width:70%;" class="fragment" data-fragment-index="2">
<b>1945-1957</b> Tubes et calculs : **programmation (binaire) par cablage**
</p>
<p style="float:right;width:30%;" class="fragment" data-fragment-index="2">
![ENIAC](images/eniac.jpeg)<!-- .element: class="plain" style="float:right;width:80%;" -->
</p>

<font style="color:red" class="fragment" data-fragment-index="3">1950 : assembleur / 1956 : Langage FORTRAN</font>
<p style="float:left;width:70%;" class="fragment" data-fragment-index="4">
<b>1958-1964</b> Transistors et **traitement par lots** (batch processing) : programmation au moyen de **cartes perforées**
</p>
<p style="float:right;width:30%;" class="fragment" data-fragment-index="4">
![IBM-1401](https://cdn.arstechnica.net/wp-content/uploads/2014/05/Hollerith_card.jpg)<!-- .element: class="plain" style="float:right;width:80%;" -->
</p>

<font style="color:red" class="fragment" data-fragment-index="5">1958 : Langage Lisp</font>
<p style="float:left;width:70%;" class="fragment" data-fragment-index="6">
<b>1965-1971</b> Circuits intégrés et **multiprogrammation** (mémoire partagée) : tâches concurrentes
</p>
<p style="float:right;width:30%;" class="fragment" data-fragment-index="6">
![PDP-1](images/pdp-col.jpeg)<!-- .element: class="plain" style="float:right;width:80%;" -->
</p>



## Histoire des ordinateurs (suite)
<br>
<font style="color:red;">1969 : Système UNIX</font>
<p style="float:left;width:75%;"  class="fragment" data-fragment-index="2">
<b>1972 - 1977</b> Circuits LSI et temps partagé : **multi-utilisateurs**</p>
<p style="float:right;width:25%;"  class="fragment" data-fragment-index="2">
![IRIS1000](images/iris1000.jpeg)<!-- .element: class="plain" style="float:right;width:50%;" -->
</p>

<font style="color:red" class="fragment" data-fragment-index="3">1973 : Langage C</font>
<p style="float:left;width:75%;" class="fragment" data-fragment-index="4">
<b>1978 - 1984</b> Micro-noyaux : **ordinateurs personnels**</p>
<p style="float:right;width:25%;" class="fragment" data-fragment-index="4">
![Apple2c](images/apple2c.jpeg)<!-- .element: class="plain" style="float:right;width:50%;" -->
</p>

<font style="color:red" class="fragment" data-fragment-index="5">1984 : Ordinateur Macintosh d'Apple</font>
<p style="float:left;width:75%;" class="fragment" data-fragment-index="6">
<b>1985 - 1995</b> Interfaces graphiques : **accessibilité**</p>
<p style="float:right;width:25%;" class="fragment" data-fragment-index="6">
![Mac 128k](images/mac128k.jpeg)<!-- .element: class="plain" style="float:right;width:50%;" -->
</p>



## Histoire des ordinateurs (suite)
<br>
<font style="color:red">1991 : Système Linux (écrit en C)</font>
<p style="float:left;width:70%;" class="fragment" data-fragment-index="2">
<b>1995 - 2007</b> Systèmes **distribués**
</p>
<p style="float:right;width:30%;" class="fragment" data-fragment-index="2">
![network](https://upload.wikimedia.org/wikipedia/commons/f/f9/NCDN_-_CDN.png)<!-- .element: class="plain" style="float:right;" -->
</p>

<font style="color:red" class="fragment" data-fragment-index="3">1999 : Linux debian 2.1 (dépôts)</font>
<p style="float:left;width:70%;" class="fragment" data-fragment-index="4">
<b>2007 - 2015</b> Systèmes **embarqués**</p>
<p style="float:right;width:30%;" class="fragment" data-fragment-index="4">
![SmartPhone](https://upload.wikimedia.org/wikipedia/commons/b/bc/Smartphone-.svg)<!-- .element: class="plain" style="float:right;width:20%;" -->
</p>

<font style="color:red" class="fragment" data-fragment-index="5">2013 : Docker (conteneurs logiciels)</font>
<p style="float:left;width:70%;" class="fragment" data-fragment-index="6">
<b>2015 - ...</b> Systèmes **virtuels**</p>
<p style="float:right;width:30%;" class="fragment" data-fragment-index="6">
![SmartPhone](https://upload.wikimedia.org/wikipedia/commons/4/4e/Docker_%28container_engine%29_logo.svg)<!-- .element: class="plain" style="float:right;width:50%;" -->
</p>



## Histoire des ordinateurs (suite)
<br>
**Tendances :**

- des super-calculateurs aux micro-systèmes  &#x2192; **miniaturisation**

- des cartes aux écrans tactiles &#x2192; **simplification** des interfaces

- du traitement par lots aux systèmes distribués virtuels

&nbsp;&nbsp;&#x2192; **externalisation** des données et logiciels (cloud)

<p class="fragment" data-fragment-index="2">
<br>**A venir (en cours) :**

<br>- internet des objets &#x2192; **''hyper''-connexion** au(x) réseau(x)

<br>- machines intelligentes &#x2192; **apprentissage** (deep learning/big data)
</p>



## <br><br><br><br><br><br>Principales fonctions d'un Système d'Exploitation
---
<br> Vues d'un SE



## SE : interface entre { utilisateur | logiciel } et matériel

<p style="width:40%;float:left;">
<br>
SE multi-utilisateurs
<br>
![](images/os_overview.png) <!-- .element: class="plain" -->
</p>

<p style="width:55%;float:right;" class="fragment" data-fragment-index="2">
<br>
SE multi-tâches
<br>
<br><br>![](images/os_blocs.png) <!-- .element: class="plain" -->
</p>
<p class="fragment" data-fragment-index="3">
&nbsp;&#x2192;&nbsp;**Partage de temps et d'espace**
</p>
<p class="fragment" data-fragment-index="3"><span style="font-size:12pt;float:right;"><br>(Figures: cours de [D. Revuz](http://www-igm.univ-mlv.fr/~dr/NCS/) et [F. Letombe](http://f.letombe.free.fr/HomePage/Teaching_files/Systemes.pdf))</span></p>



## Composition

<p style="width:60%;float:left;">
![](images/os_architecture.png)<!-- .element: class="plain" style="width:80%;" -->
</p>
<p style="width:40%;float:right;">
- Utilisateurs du SE sont **humains et / ou logiciels**
<br><br>
- Interface du SE unifiée :<br>**appels systèmes**
<br><br>
- Le SE gère :<br>&nbsp;+ les **programmes<br>&nbsp;&nbsp;&nbsp; en cours d'exécution**, <br>&nbsp;+ les **fichiers**, <br>&nbsp;+ les **périphériques** (via<br>&nbsp;&nbsp;&nbsp; les pilotes)
</p>



## <br><br><br><br><br><br>Principaux Systèmes d'Exploitation
---
<br> Systèmes les plus utilisés actuellement



## Quelques statistiques (PC de bureau)
<br>
SE les plus utilisés (en %)
<canvas style="float:right;" class="stretch" data-chart="pie">
, Windows XP, Windows Vista, Windows 7, Windows 8, Windows 10, MacOS X, Linux 
PC de bureau, 9.17, 0.84, 47.20, 8.52, 25.30, 6.32, 2.27
center, 0,0,0,0,0,0,0
</canvas>
&nbsp;<span style="font-size:12pt;float:right;">Source: [Enquête VenturaBeat](https://venturebeat.com/2017/02/01/windows-10-passes-25-market-share-vista-drops-below-1/) (Jan. 2017)</span>



## Quelques statistiques (Smartphones)
<br>
SE les plus utilisés (en %)
<canvas style="float:right;" class="stretch" data-chart="pie">
, Android, iOS, Blackberry, Windows Phone, Autre
Smartphone, 52, 35, 7, 2, 5
center, 0,0,0,0,0
</canvas>
&nbsp;<span style="font-size:12pt;float:right;">Source: [Enquête Nielsen](http://www.iphonehacks.com/wp-content/uploads/2012/12/Q3-2012-US-Smartphone-OS-market-share.png) (Dec. 2012)</span>



## Quelques statistiques (Serveurs)
<br>
SE les plus utilisés (en %)
<canvas style="float:right;" class="stretch" data-chart="pie">
, Linux, Windows, FreeBSD, Solaris, AIX, HP-UX, OpenBSD, MacOS X
Serveur, 64, 20, 7, 4, 2, 1, 1, 1
center, 0,0,0,0,0,0,0,0
</canvas>
&nbsp;<span style="font-size:12pt;float:right;">Source: [Enquête Nielsen](http://www.iphonehacks.com/wp-content/uploads/2012/12/Q3-2012-US-Smartphone-OS-market-share.png) (Dec. 2012)</span>



## Principaux Systèmes d'Exploitation
<br>
**Caractéristiques** :

- multi-tâches 

- multi-utilisateurs

- multi-bureaux

- ouverts (ou non)

- gratuits (ou non)

- stables (ou non)

- compatibles (matériels), cf pilotes pré-installés (ou non)

- ergonomiques (&#x26A0; intuition vs habitude)



## <br><br><br><br><br><br>Linux
---
<br>Principales caractéristiques



## Linux(es)

![Linux](https://upload.wikimedia.org/wikipedia/commons/7/77/Unix_history-simple.svg) <!-- .element: class="plain" style="width:90%;" -->



## Linux
<br>
(+) Interactivité riche (environnements graphiques + terminal)

(+) Grande communauté de développeur.euse.s

(+) Nombreux outils systèmes (éditeurs, ...) pré-installés

(+) Ecrit dans un langage de haut niveau (C)

(+) Evolution continue (deux distributions / an pour Ubuntu)

(+) Distribué sous licence "libre" (e.g. GPL / BSD)

(+) Très documenté

---

(-) Compatibilité limitée avec certains matériels

(-) Stabilité (de certaines distributions / versions)



## <br><br><br><br><br><br>Les composants principaux du SE
---
<br>Gestionnaires de Processus / Mémoire / Fichiers / Entrées-Sorties



## <br><br><br><br><br><br>Gestion des processus
---
<br><!-- Ordonnancement, redirections, signaux, threads -->



## Gestion des processus
<br>
- **Processus** : programme **en cours d'exécution** <br>(chargé en mémoire)

- Identifié par un **PID**

- Associé à un **espace d'adressage** (mémoire) contenant :
  - le programme exécutable (instructions)  
  - les données 
  - la pile (des appels)
  
- Associé à des **registres** (variables) dont :
  - pointeur de pile
  - pointeur d'instruction (*aka* compteur ordinal)



## Gestion des processus (suite)
<br>
- Processus **créé** (*fork*) lors :

  - de l'**initialisation** du système (démarrage)
  
  - d'un **appel système** (utilisateur ou logiciel)
  
  - travail **programmé** (ex. traitement par lots)

- Processus créateur : processus **parent** (PPID)

- Bénéficie d'une **allocation** de ressources (mémoire, CPU, ...)

- Plusieurs processus peuvent être chargés en mémoire conjointement : **multi-programmation**



## Gestion des processus (suite)
<br>
- Processus **terminé** par :

  - **fin** conforme de la tâche (*instruction 'exit'*)
  
  - fin non-conforme **attendue** de la tâche (e.g., div. par zéro)
  
  - fin non-conforme **inattendue** de la tâche (e.g., prob. réseau)
  
  - fin **provoquée** par un { utilisateur | logiciel } (*signal 'kill'*)

- par défaut, l'arrêt d'un processus **entraîne l'arrêt de ses processus fils** 

- un processus peut être **interrompu** (*'sleep'* $\approx$ en pause) et éventuellement **redémarré** (*'wake up'*) par la suite



## Gestion des processus (suite)

<p style="width:47%;float:left;">
<br>- Processus gérés par un **ordonnanceur** (*préemptif* ou *non-préemptif*)
<br>
<br>- Allocation du processeur dépend de diverses info. (*priorité*, estimation de *durée*, disponibilité des *ressources* demandées, etc.)
<br>
<br>- Cycle de vie du processus : différents **états** (prêt, bloqué, <br>en cours d'exécution)
</p>
	<p style="width:52%;float:right;">
<br>![etats](https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Diagramme_etat_processus.svg/728px-Diagramme_etat_processus.svg.png) <!-- .element: style="width:70%;float:right;" -->
</p>



## Gestion des processus (suite)
<br>
Quelques algorithmes d'**ordonnancement** (*scheduling*) :

 - Premier arrivé, premier servi : *First Come, First Served*
 
 - Tâche la plus rapide en premier : *Shortest Job First*
 
 - Tourniquet : *Round Robin* <br>(file d'attente circulaire avec quantum alloué au processus élu)
 
 - File d'attente multi-niveaux : *Multi-level Feedback Queue*

**Contraintes** :

 - maintenir le processeur occupé
 
 - ne pas retenir un processus en file d'attente trop longtemps
 
 - prendre en compte le temps de *commutation*



## Communication entre processus : entrées / sorties
<br>
**Flux de données** (entrée / sortie standard et sortie des erreurs) :
<center>
![](https://camo.githubusercontent.com/18e02a1a7735857d9745649f5a02408a879f51e1/68747470733a2f2f646f63732e676f6f676c652e636f6d2f64726177696e67732f642f3169655449476b3539306a79684f596b3263654c6354782d466e44366361474c4b587955594241656b5275512f7075623f773d36393026683d333633) <!-- .element: class="plain" style="width:50%" -->
</center>
<p class="fragment" data-fragment-index="2">
&#x2192; Les flux peuvent être **redirigés** (dans un fichier, à la "poubelle", vers une *socket* réseau, etc.) 
<br>
&#x2192; Les processus peuvent s'enchainer (pour former un **pipeline**) via redirection de l'entrée / sortie appelée aussi **tube** [nommé]
</p>



## Communication entre processus : les signaux

- **Signal** : notification asynchrone envoyée à un processus pour lui signaler l'apparition d'un événement<span style="font-size:12pt;float:right;"><br>(source: [wikipedia](https://fr.wikipedia.org/wiki/Langage))</span>

- Principaux signaux :
<center>
![](https://i.stack.imgur.com/9EQBC.png)<!-- .element: class="plain" style="width:80%;" -->
</center>



## Processus légers (threads)
<br>
<p class="fragment" data-fragment-index="1" style="float:left;width:47%">
**Processus :**
<br>1. Pointeur
<br>2. État du processus
<br>3. Numéro du processus
<br>4. Compteur d’instructions
<br>5. Registres
<br>6. Limite de la mémoire
<br>7. Liste des fichiers ouverts
<br>8. ...
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:47%">
**Thread :**
<br>1. un compteur d'instructions
<br>2. un ensemble de registres
<br>3. une pile
<br>
<br>**Un thread partage (avec des threads compatibles) :**
<br>1. la section de code
<br>2. la section de données
<br>3. les ressources du SE
</p>



## <br><br><br><br><br><br>Gestion de la mémoire
---
<br><!-- Définition, organisation -->



## Mémoire
<br>
- Zone de stockage de l'information (sous forme binaire)

- Principales aractéristiques :

  - taille
  - rapidité d'accès
  - volatilité
  - coût
  
- Différents types :

  - mémoire vive (RAM) vs mémoire morte (ROM)
  - mémoire cache
  - disque dur
  - support amovible ou distant
  - etc.



## Mémoire (suite)

<br>-&nbsp;Espace divisé en **blocs** (*mots mémoire*) de taille fixe possédant chacun une adresse propre

-&nbsp;Taille des mots dépendante de l'**architecture** (registres, bus)

-&nbsp;Lors de l'exécution d'un programme, les instructions à exécuter sont **chargées** en mémoire vive, puis envoyées une à une au processeur (cf compteur d'instruction)

-&nbsp;**Problèmes** :
  - à quelle adresse stocker un programme ? 
  - si un processus demande dynamiquement de la mémoire ?
  - comment empêcher un processus d'écrire dans une zone d'un autre processus ?

<p class="fragment" data-fragment-index="1">&#x2192; Introduction d'une couche d'abstraction : **mémoire virtuelle**</p>



## Mémoire et multi-programmation
<br>
- Partage du processeur entre processus <br>&#x21D2; **partage** de la mémoire vive

- Si trop de processus actifs &#x2192; stockage de certains processus sur le disque : **swapping** (*aka* va-et-vient) 

- Chargement des processus en mémoire : **file d'attente** et décomposition de la mémoire en **partitions** 

 - soit de taille fixe (nb limite de processus, espaces perdus)
 - ou de taille variable (coût de gestion, fragmentation)

- Gestion de la fragmentation : **compactage** (appelé aussi défragmentation, opération coûteuse)



## Allocation de mémoire
<br>
- Gestion des allocations dynamiques de la **mémoire** (a) <br>&#x2192; **tables de bits** (b) ou **liste chaînée** (c)<br><br>
  
<center>
![](images/memory_management.png)
</center>



## Allocation de mémoire (suite)
<br>
(1) **Table de bits** vs (2) **liste chaînée**

- Avantage de (1) :
  - index stocké dans un espace de taille finie

- Inconvénients de (1) :
  - Difficulté de définir la taille de l'unité mémoire (perte de place pour la table vs perte de place dans les blocs)
  
  - Coût (algorithmique) de recherche d'un segment libre de taille suffisante



## Allocation de mémoire (suite)
<br>
Principaux **algorithmes d'allocation** par liste chaînée :<br><br>

 - Allocation du premier trou suffisamment grand : *First-fit*
 
 - Allocation du premier trou suffisamment grand à partir de la position courante : *Next-fit*
 
 - Allocation du plus petit trou de taill suffisante : *Best-fit*
 
 - Allocation du trou le plus grand : *Worst-fit*
 
<br>NB: un tri des trous par taille peut améliorer l'efficacité des algorithmes d'allocation



## Mémoire virtuelle
<br>
- Principe : offrir un **espace mémoire virtuel** "indépendant" de l'espace physique disponible 

  - on peut ainsi ne charger qu'un **fragment de programme** en mémoire physique
  - le développeur n'a **pas à se préoccuper** de la place réellement disponible

- **Espace d'adressage virtuel** découpé en **pages** de taille fixe

- **Espace d'adressage physique** découpé en **cadres de pages** (*frames*) de même taille

- Adresse *logique* vs adresse physique : traduction via une **table des pages**



## Adressage virtuel
<br>
<p style="width:50%;float:left">
Exemple de mots de 16 bits :
<br>
<br>&#x2192; $2^{16}$ adresses disponibles en mémoire virtuelle
<br>
<br>&#x2192; soit 64 ko de mémoire virtuelle divisée en blocs de 4 ko
<br>
<br>Associée ici à une mémoire physique de 32 ko :
</p>
<p style="width:46%;float:right">
![](images/pageframe.png)<!-- .element: class="plain" style="width:80%;" -->
</p>



## Table des pages
<br>
<p style="width:46%;float:left;"> 
![](images/table_pages.png) <!-- .element: class="plain" -->
</p>
<p style="width:46%;float:right;">
![](images/pagination.png) <!-- .element: class="plain" -->
</p>
<br><br><br><br><br><br><br><br><br><br><br><br>
<span style="font-size:12pt;float:right;"><br>(Figures: cours de [D. Revuz](http://www-igm.univ-mlv.fr/~dr/NCS/) et [M. Halfeld](https://www.univ-orleans.fr/lifo/Members/Mirian.Halfeld/Cours/SEBlois/SE2007-GestionMemo.pdf))</span>



## Table des pages (suite)
<br>
- Si adresses virtuelles sur 32 bits &#x21D2; table de pages comptant près d'un million d'entrées !

- Coût de mise à jour de la table des pages !

- Implantation : 
  - *Page Table Base Register* (PTBR) en mémoire principale
  - *Translation Lookaside Buffer* (TLB) en mémoire cache 

- Commutation de contexte &#x2192; modification de la PTBR

- SE moderne : adressage sur 64 bits &#x2192; table des pages très grande 
  &#x21D2; pagination à plusieurs niveaux



## Table des pages (suite)
<center>
![](images/multipages.png) <!-- .element: class="plain" style="width:65%;" -->
</center>



## Table inversée

- Entrée associant à un cadre la *page correspondante* et des *informations
  sur le processus* auquel appartient cette page

- Avantage : une seule table de pages dans le système (réduction de
  la quantité de mémoire requise pour stocker les tables)

- Inconvénient : augmentation du temps nécessaire pour
chercher la table quand il se produit une référence à une page
<p class="fragment" data-fragment-index="1" style="width:35%;float:left;">&#x2192; Utilisation d'une **table de hachage** (*hash table*)</p>
<p class="fragment" data-fragment-index="1" style="width:60%;float:right;">![](images/table_inversee.png) <!-- .element: class="plain" --></p>



## Remplacement de page
<br>
- Quand une page n'est pas en mémoire :
  1. Trouver la page désirée sur le disque
  2. Trouver un cadre de page libre :

    - S'il n'y en a pas, utiliser un algorithme de **remplacement de pages** (FIFO, Least-Recently Used, Comptage, etc.) pour sélectionner un cadre "victime"

  3. Sauvegarder le cadre victime sur le disque et charger la page désirée dans le cadre libéré (mettre à jour les tables)
	
  4. Redémarrer le processus utilisateur



## <br><br><br><br><br><br>Gestion des fichiers
---
<br><!-- Notions de système de fichier, format, inode -->



## Gestion des fichiers
<br>
- **Fichier** : unité logique de stockage d'information, enregistrée dans une mémoire *pérenne*

- Deux points de vue :

 1. **Utilisateur** : type (format), organisation, (droits d') accès
 
 2. **Système** : structure, stockage, (rapidité d') accès
 
- Fichier composé de **méta-données** (<strike>nom</strike> chemin, taille, date de création, date de modification, propriétaire, droits d'accès, etc.) et d'un **contenu** (octets)

- Deux opérations de base : **lecture** / **écriture**

- Deux modes d'accès : par **caractères** / **octets** (cf interprétation)



## Organisation logique des fichiers
<br>
- Fichiers organisés en **partitions** (disques virtuels), contenant chacune une **arborescence** de fichiers 

- Sous windows : racine de la partition principale notée **C:**
  <br>Sous linux : racine de la partition principale notée **/**

- Sous linux : 

  - les partitions secondaires sont **montées** dans l'arborescence principale (noeud feuille remplacée par la racine de l'arborescence secondaire)

  - partition appelée **swap** sert d'extension de la mémoire vive
  
  - système et données sont généralement installées sur des partitions distinctes (données montées sur **home/**)



## Organisation logique des fichiers (suite)
<br>
- Noeuds feuilles de l'arborescence correspondent aux **fichiers**
  <br>noeuds internes correspondent aux **répertoires** ($\approx$ fichiers)
  
- Pour chaque fichier / répertoire, sous linux : 
  - trois **niveaux d'accès** (lecture / écriture / exécution)
  - trois **types d'utilisateurs** (user / group / others)

- Pour les répertoires :
  - lisible &#x21D2; contenu **listable**
  - exécutable &#x21D2; répertoire **traversable**

- Chemin d'un fichier est **relatif ou absolu** et composé d'une liste de noeuds séparés par un **\** (windows) ou un **/** (linux), exemple:
<center>
`/usr/local/bin/decktape`
</center>



## Organisation logique des fichiers (suite)
<br>

<center>
![](https://upload.wikimedia.org/wikipedia/commons/f/f3/Org_Chart_Cacoo_Linux_Tree_John.png) <!-- .element: class="plain" -->
</center>



## Organisation physique des fichiers
<br>
- Organisation correspondant au **système de fichiers** (*FS*)

- Gestion notamment de la **sauvegarde / intégrité** des données (cf journalisation), de la **sécurité** (cf chiffrement), des **accès concurrents**

- Chaque partition est structurée (**formattée**) de manière propre, principaux **formats** : FAT, FAT32, NTFS, Ext3, Ext4, ReiserFS, ...

- FS implanté sous forme de **blocs** (cf coût d'accès)

- FS offre une interface d'**accès efficace** aux données (par exemple via structures de données adéquates)

- Deux méthodes d'**allocation** de blocs : **contiguë** ou **par listes chaînées**



## Organisation physique des fichiers (suite)
<br>
- Association entre fichiers et bloc(s) correspondant(s) stockées dans une **table d'allocation** :

  - **FAT** (windows) : table contenant l'adresse du premier bloc *de chaque fichier* 
  - **i-noeuds** (linux) : table contenant les attributs et adresses des blocs *pour un fichier* 
  
- Table FAT stockée en début de partition
  
- I-noeuds stockés sur le disque au moyen de structures permettant un accès rapide (arbres lexicographiques, tables de hachage, etc.)



## Organisation physique des fichiers (suite)
<br>
Structure des inoeuds :
<center>
![](https://upload.wikimedia.org/wikipedia/commons/0/09/Ext2-inode.svg) <!-- .element: class="plain" style="width:65%" -->
</center>



## <br><br><br><br><br><br>Gestion des entrées / sorties
---
<br><!-- Quelques précisions -->



## Entrées / sorties

- Gestion des **périphériques** d'entrées/sorties :

  - émission des commandes vers les périphériques

  - interception des interruptions

  - gestion des erreurs

- **Interface** identique pour tous les périphériques

- Deux **types** de périphériques : 

  - périphériques échangeant l'information par **blocs** (cas du disque par exemple) 
  
  - périphériques échangeant l'information par **flux de caractères** (cas du clavier ou de la souris par exemple)



## Entrées / sorties (suite)
<br>
- Certains périphériques sont :

 - équipés de **tampons** (*buffer*) 
 <br>&#x2192; *mémoire interne* pour l'échange de données

 - associés à un **contrôleur**
 <br> &#x2192; *registres* permettant une communication avec le processeur



## <br><br><br><br><br><br>Virtualisation
---
<br>



## Virtualisation
<br>
- **Principe** : faire fonctionner plusieurs systèmes sur une même architecture matérielle

- **Objectifs** :
  - faciliter l'administration (maintenance / installation d'applications, indépendances par rapport au matériel, etc.)

  - réduire les coûts (mutualisation)

- **Deux types** de virtualisation principaux :

  - *hyperviseurs* (virtualisation système, ex. VirtualBox)

  - *conteneurs* (virtualisation applicative, ex. Docker)



## Hyperviseurs de type 1
<br>
<p style="width:50%;float:left">
- noyau **(système) hôte** très léger et optimisé
<br>
<br>- gestion directe des accès aux ressources pour les **systèmes invités**
<br>
<br>- offre de **bonnes performances**
<br>
<br>- Exemples : MS Hyper V Server, KVM, Citrix Xen server, ...
</p>
<p style="width:50%;float:right">
![](https://upload.wikimedia.org/wikipedia/commons/f/fa/Diagramme_ArchiHyperviseur.png)<!-- .element: class="plain" -->
</p>



## Hyperviseurs de type 2
<br>
<p style="width:50%;float:left">
- **logiciel** exécuté sur le système hôte
<br>
<br>- systèmes invités **installés** (de manière classique) sur un émulateur de PC
<br>
<br>- offre des **performances limitées** (trois couches)
<br>
<br>- Exemples : VMware, VirtualBox, Parallels Desktop, ...
</p>
<p style="width:50%;float:right">
![](https://upload.wikimedia.org/wikipedia/commons/9/99/Diagramme_ArchiEmulateurNonNatif.png)<!-- .element: class="plain" -->
</p>



## Conteneurs
<br>
<p style="width:50%;float:left">
- **logiciel** permettant d'isoler l'exécution d'applications (contextes)
<br>
<br>- **facilite le partage de ressources** entre applications
<br>
<br>- offre de **très bonnes performances**
<br>
<br>- Exemples : Docker, chroot, LXC, BSDjail, OpenVZ, ...
</p>
<p style="width:50%;float:right">
![](https://upload.wikimedia.org/wikipedia/commons/3/38/Diagramme_ArchiIsolateur.png)<!-- .element: class="plain" -->
</p>



## Autres types de virtualisation
<br>
- *processeur* virtuel (cf architecture à la Java)

- *mémoire* virtuelle (cf supra)

- *disques* virtuels (cf points de montage)

- *données* virtuelles (cf systèmes de gestion de bases de données)

- *réseaux* virtuels (cf VPN)

- *environnements applicatifs* virtuels (cf applications portables *à la* snappy, flatpak, etc.)



## Virtualisation et internet

Prédominance des **services**
<center>
![](https://media.licdn.com/dms/image/C4E12AQH3p44mG-q4Wg/article-inline_image-shrink_1000_1488/0?e=1567036800&v=beta&t=8xVK7mUCQXlfoVArwU8K88sXsnGJHS5CPQmhPerfH7I) <!-- .element: class="plain" style="width:70%" -->
</center>



## <br><br><br><br><br><br>Le Shell
---
<br><!-- Comment intéragir directement avec le SE -->



## Le shell
<br>
- Aussi connu sous le nom de **Terminal**

- **Interpréteur** de commandes

- Langage par défaut : **Bash** (Bourne Again SHell, 1989)

- Permet d'interagir avec le SE via des (compositions de)
  **commandes**, et des **scripts**

- Au **lancement**, le shell se place dans le répertoire "maison" (chemin '`/home/<mon_login>/`' ayant pour alias '`~`')

- Format du **prompt** : 
<center>
`<mon_login>@<ma_machine>:<chemin_actuel>/$`
</center>



## Bash

- **Variables** *exportables* définies via (pas d'espace autour de `=`) : 
<center><font style="color:orange">`[export] IDENTIFIANT=VALEUR`</font></center>

- Données **non typées** (valeurs sont des *chaînes de caractères*)

- Pour référer au contenu d'une variable : 
<center><font style="color:orange">`$IDENTIFIANT`</font> &nbsp; ou &nbsp; <font style="color:orange">`${IDENTIFIANT}`</font></center>

- **Variables d'environnement** (pré-définies au niveau du système ou dans `~/.bashrc`) incluent :
 
   - <font style="color:orange">`LOGNAME`</font> : login utilisateur
   - <font style="color:orange">`SHELL`</font> : chemin vers l'interpréteur shell par défaut
   - <font style="color:orange">`HOME`</font> : chemin vers le répertoire maison
   - <font style="color:orange">`PATH`</font> : chemins vers les programmes (dont commandes)
   
- **Commande** permettant de lister ces variables : <font style="color:orange">`env`</font>



## Commandes de base
<br>
 - <font style="color:orange">`pwd`</font> : afficher la position courante (dans l'arborescence)

 - <font style="color:orange">`whoami`</font> :  afficher le login

 - <font style="color:orange">`echo $X`</font> : afficher la valeur de X

 - <font style="color:orange">`cd <chemin>`</font> : aller au chemin (*Change Directory*)
 
 - <font style="color:orange">`ls <chemin>`</font> : lister le contenu du chemin (répertoire) 
 
   - <font style="color:orange">`.`</font> (répertoire courant, chemin par défaut)
   - <font style="color:orange">`..`</font> (répertoire parent)
   
 - <font style="color:orange">`cat <chemin>`</font> : afficher le contenu du chemin (fichier)
 
 - <font style="color:orange">`wc <chemin>`</font> : afficher les stats (lignes, mots, caractères) 



## Commandes de gestion de processus
<br>
- <font style="color:orange">`ps u`</font> : afficher les processus lancés par l'utilisateur connecté

- <font style="color:orange">`pstree`</font> : afficher l'arbre généalogique des processus

- <font style="color:orange">`top`</font> : afficher les processus en cours et les ressources allouées (taper **`q`** pour revenir au mode interactif du terminal)

- <font style="color:orange">`kill -<signal> <pid>`</font> : envoyer le signal au processus

- <font style="color:orange">`nice -n <nb> <command>`</font> : ajouter nb unités à la priorité de la commande 



## Commande de gestion de processus (suite)

<center>
![](images/psaux.png) <!-- .element: class="plain" style="width:90%" -->
</center>



## Commandes de gestion de fichiers
<br>
- <font style="color:orange">`cp <source> <destination>`</font> : copie de fichier (avec éventuellement renommage si destination est un fichier)

- <font style="color:orange">`mv <source> <destination>`</font> : déplacement de fichier (avec éventuellement renommage si destination est un fichier)

- <font style="color:orange">`rm <chemin>`</font> : suppression de fichier

- <font style="color:orange">`rm -r <chemin>`</font> : suppression de répertoire

- <font style="color:orange">`mkdir <chemin>`</font> : création de répertoire

- <font style="color:orange">`chown -R <login>:<group> <chemin>`</font> : changer le propriétaire (et groupe) du chemin (récursivement)

- Les chemins peuvent contenir des **jockers** (<font style="color:orange">`* ?`</font>) substitués



## Commandes d'administration
<br>
- <font style="color:orange">`chmod <droits> <chemin>`</font> : définir les droits sur le chemin, droits étant de la forme :
  - `X+Y` (donner le droit Y à X) 
  - `X-Y` (retirer le droit Y à X) 
   
  `X` vaut '`u`' (user), '`g`' (group), '`o`' (others) ou (non exclusif) '`a`' (all)
  
  `Y` vaut '`r`' (read), '`w`' (write) ou '`x`' (execute)

- <font style="color:orange">`sudo <commande>`</font> : exécuter la commande en tant qu'administrateur (*Super User DOes*)

- <font style="color:orange">`useradd <login>`</font> : créer un utilisateur 

- <font style="color:orange">`passwd`</font> : changer de mot de passe



## Format des commandes
<br>
<center>
<font style="color:orange">`commande [option]... [argument]...`</font>
</center>

- **Options** éventuellement collées et précédées du signe '`-`'

- **Arguments séparés par des espaces** (&#x26A0; noms de fichiers)

- Exemples :
  - <font style="color:orange">`ls -alh`</font> (lister les fichiers cachés ou non, avec les infos)
  - <font style="color:orange">`cp -r <chemin> <dest>`</font> (copier récursiv<sup>t</sup> le chemin) 
  - <font style="color:orange">`cut -d';' -f1,2 <chemin>`</font> (couper les lignes) 
  - <font style="color:orange">`mv toto tata tutu ~/Documents/`</font> <br>(déplacer plusieurs fichiers)

- Pour accéder à la documentation (au manuel) d'une commande&nbsp;: <font style="color:orange">`man commande`</font>



## Commandes de recherche de fichiers
<br>
- P/r aux **méta-données** : <font style="color:orange">`find <depart> <criteres>`</font>

- P/r au **contenu** : <font style="color:orange">`grep <patron> [<chemins>]`</font>

- **Exemples** :

  <font class="fragment" data-fragment-index="1" style="color:orange">`find ~ -type d -ctime +60`</font>

  <font class="fragment" data-fragment-index="2"  style="color:orange"><br><br>`find ~ -type f -size -40k`</font>
  
  <font class="fragment" data-fragment-index="3"  style="color:orange"><br><br>`find ~ -iname "*.pdf" -exec lpr {} \;`</font>
  
  <font class="fragment" data-fragment-index="4"  style="color:orange"><br><br>`grep "^[A-Z]+.*$" *.py`</font>

  <font class="fragment" data-fragment-index="5"  style="color:orange"><br><br>`grep "[a-z]{4,}" *.cpp`</font> &nbsp; ; &nbsp; <font class="fragment" data-fragment-index="5"  style="color:orange">`grep -E "(a|b)u" *.txt`</font>



## Redirections
<br>
- Lors de l'exécution d'une commande, un processus est créé. Trois **flux** vont être ouverts :

  - **stdin** l'entrée standard (par défaut le clavier)
identifiée par l'entier **0** (descripteur)
  - **stdout** la sortie standard (par défaut l'écran)
identifiée par l'entier **1**
  - **stderr** la sortie d'erreur standard (par défaut
l'écran) identifiée par l'entier **2**

- Exemple d'envoi vers **stdout** : <font style="color:orange">echo $?</font>

- Exemple de lecture dans **stdin** : <font style="color:orange">read X</font>

- Chaque flux peut être **redirigé** vers un fichier, une socket, etc.



## Redirections (suite)
<br>
Redirection des flux d'E/S au moyen d'**opérateurs** spécifiques :

  - <font style="color:orange">X **`>`** Y</font> &nbsp;&nbsp;&nbsp;&nbsp;(redirection de la **stdout** de X dans un fichier)

  - <font style="color:orange">X **`<`** Y</font> &nbsp;&nbsp;&nbsp;&nbsp;(redirection de la **stdin** de X depuis un fichier)
  
  - <font style="color:orange">X **`>>`** Y</font> &nbsp;&nbsp;(redirection de la **stdout** de X dans un fichier en ajout)
  
  - <font style="color:orange">X **`>&`** Y</font> &nbsp;&nbsp;(redirection de **stdin ET stderr** de X dans un fichier)
  
  - <font style="color:orange">X **`|`** Y</font> &nbsp;&nbsp;&nbsp;&nbsp;(redirection de **stdout** de X vers **stdin** de Y)

<p class="fragment" data-fragment-index="1">
**Exemples** :
  <br>&nbsp;&nbsp;&nbsp;&nbsp; <font style="color:orange">`ls -l /etc > listing.txt`</font>
  <br>&nbsp;&nbsp;&nbsp;&nbsp; <font style="color:orange">`wc -c < /tmp/a.out`</font> &nbsp;&nbsp;&nbsp;&nbsp; (<font style="color:orange">`cat /tmp/a.out | wc -c`</font>)
  <br>&nbsp;&nbsp;&nbsp;&nbsp; <font style="color:orange">`echo /tmp/a.out |xargs wc -w  `</font> &nbsp;&nbsp; (<font style="color:orange">`wc -w /tmp/a.out`</font>)
</p>



## Evaluation de commandes
<br>
- Le résultat d'une commande peut être *calculé* dynamiquement (pour substitution) au moyen de **`$( )`** ( ou **` `` `**), exemple :

<center>
<br><font style="color:orange">`grep "^import" $(find ~ -iname "*.py")`</font><br><br>
</center>

- Pour calculer une expression arithmétique entière, il faut utiliser **`$(( ))`**, exemple : 

<center>
<br><font style="color:orange">`echo $(( 2 * 3 + 4 ))`</font><br><br>
</center>

- Pour stocker le résultat d'une commande dans une variable <br>(`" "` sont utilisés pour protéger les éventuels espaces) :

<center>
<br><font style="color:orange">`X="$(cat toto | head -1)"`</font><br><br>
</center>



## Pour aller plus loin
<br>
- Bash: véritable langage généraliste, cf. **structures de contrôle** (instructions *si alors sinon*, boucles *pour* et *tant que*), **fonctions**
<br>&#x2192; Voir [transparents de cours spécifiques [&#x2197;]](http://www.univ-orleans.fr/lifo/membres/Yannick.Parmentier/linux/support2.pdf)

- Difficultés : 
  - langage **non-typé**
  - rôle prépondérent des caractères **espace** et **retour-chariot** 

- Avantages : 
  - disponible **nativement** (pas d'installation nécessaire)
  - **flexibilité** (redirections, interactions avec le système)

- Alternatives : **Perl**, **Python**



## Pour aller plus loin (suite)
<br>
- Bash :
<p>
<pre><code>#! /bin/bash
rep=$1
cd $rep
for file in $(ls *.py) ;
do
    X=$(cat $file | head -1)
    echo $X
done
</code></pre>
</p>

- Python :
<p>
<pre><code class="python">#! /usr/bin/env python3
import os, sys, glob
rep = sys.argv[1]
os.chdir(rep)
for file in glob.glob('*.py'):
	with open(file, 'r') as fdesc:
	    print(fdesc.readline().strip())
</code></pre>
</p>



## Conclusion
<br>
- **Principales fonctions** d'un SE :

  - gestion des processus
  - gestion de la mémoire
  - gestion des fichiers
  - gestion des périphériques

- **Tendances** : 
  
  - miniaturisation 
  - externalisation 
  - virtualisation



## Références
<br>
- *Système* (2006) Florian Letombe. Transparents de cours. IUT d'Artois. [[&#x2197;](http://f.letombe.free.fr/HomePage/Teaching_files/Systemes.pdf)]

- *Systèmes d'Exploitation* (2009) Mirian Halfeld. Transparents de cours. Université de Tours [[&#x2197;](http://www.univ-orleans.fr/lifo/Members/Mirian.Halfeld/mi-SE-L2.html)]

- *Cours de Système* (2014). Université de Savoie. [[&#x2197;](http://www.lama.univ-savoie.fr/mediawiki/index.php/INFO502_:_Syst%C3%A8mes_d%27exploitation)]

- *Virtualisation* (2019) Lucas Nussbaum. Transparents de cours. IUT Nancy Charlemagne. [[&#x2197;](https://members.loria.fr/LNussbaum/asrall/virt1-intro-lxc.pdf)]

- *Cours de Système* (2005) David Revuz. Polycopié de cours. Institut Gaspard Monge, Université Paris Est. [[&#x2197;](http://www-igm.univ-mlv.fr/~dr/NCS/)]



# <br><br><br><br><br>Merci pour votre attention



## Exercice

<iframe class="stretch" data-src="code/td_bash.pdf"></iframe>

<small style="font-size:10pt;float:right;">Solutions: [Ex.1.1](code/calculatrice.py), [Ex.2](code/calculatrice2.py), [Ex.3](code/calculatrice3.py)</small>
