from lark import Lark, Transformer, tree

json_parser = Lark(r"""
    ?value: dict
          | list
          | string
          | SIGNED_NUMBER      -> number
          | "true"             -> true
          | "false"            -> false
          | "null"             -> null

    list : "[" [value ("," value)*] "]"

    dict : "{" [pair ("," pair)*] "}"
    pair : string ":" value

    string : ESCAPED_STRING

    %import common.ESCAPED_STRING
    %import common.SIGNED_NUMBER
    %import common.WS
    %ignore WS

    """, start='value')

class MyTransformer(Transformer):
    def list(self, items):
        return list(items)
    def pair(self, apair):
        return apair
    def dict(self, items):
        return dict(items)

class TreeToJson(Transformer):
    def string(self, s):
        return s[0][1:-1]
    def number(self, n):
        return float(n[0])

    list = list
    pair = tuple
    dict = dict

    null = lambda self, _: None
    true = lambda self, _: True
    false = lambda self, _: False

text = '{"key": ["item0", "item1", 3.14, true]}'
print( json_parser.parse(text).pretty() )

ast = json_parser.parse(text)
print(ast)

code = MyTransformer().transform(ast)
print(code)

tree.pydot__tree_to_png( json_parser.parse(text), 'aout.png')

code2 = TreeToJson().transform(ast)
print(code2)
