#! /bin/bash
# Programme servant à faire des recherches dans
# l'index de Gutenberg
# Contact: yannick.parmentier@loria.fr
# Date: 2019/07/16
#######################################

# Préparation du pipeline de recherche par mots clés
cmd="cat GUTINDEX.ALL | grep -E \", by.*[0-9]+\s*$\" "

# Pour chaque mot parmi les paramètres du script
for mot in $*;
do
    cmd="$cmd | grep -i $mot"
done

# On supprime les retours chariots DOS (\r),
# et on extrait les références d'oeuvres
cmd="$cmd | tr -d \"\\r\" | tr -s \" \" | rev | cut -d' ' -f1 | rev " 
#echo $cmd

# On évalue la requête (pipeline) et on envoie le résultats sur la sortie standard
X=$(eval $cmd)
# Si la commande ne retourne rien :
if [ "$X" = "" ];
then
    # Message d'erreur
    echo "Aucune oeuvre trouvée" >> /dev/stderr # envoyé uniquement sur stderr
else
    # Informations envoyées sur stdout (défaut)
    echo $X | tr ' ' '\n' # ne pas oublier de remettre des retours chariots (\n) au lieu des espaces pour la boucle qui suit dans getBook.sh
fi
