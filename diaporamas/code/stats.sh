#! /bin/bash
# Contact: yannick.parmentier@loria.fr
# Date: 2019/07/16
###########################################

# Si log non présent
if [ ! -f ~/.gutenberg.log ];
then
    touch ~/.gutenberg.log
fi

# Récupération des statistiques demandées :
NBLIVRES=$(cat ~/.gutenberg.log | grep "^Title:" | wc -l)
NBAUTEURS=$(cat ~/.gutenberg.log | grep "^Author:" | cut -d':' -f2 | sort -u | wc -l)
AUTEURS=$(cat ~/.gutenberg.log | grep "^Author:" | cut -d':' -f2 | sort -u)

# Affichage du résultat sur la sortie standard au format demandé
echo -n "$NBLIVRES " # -n pour qu'il n'y ait pas de \n final
echo -n "$NBAUTEURS "
echo "$AUTEURS" | tr -d '\r' | tr '\n' ' '
