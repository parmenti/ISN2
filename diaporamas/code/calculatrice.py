import sys
from lark import Lark, tree

grammar = """
    ?start: expression
            | NAME "=" expression        -> assign

    expression: pexp
            | npexp
            | atom
            | "(" expression ")"

    pexp: expression "*" expression      -> mult
            | expression "/" expression  -> div

    npexp: expression "+" expression     -> add
            | expression "-" expression  -> sub

    atom: NAME                           -> var
            | NUMBER                     -> val
            | "(" "-" atom ")"           -> neg

    %import common.INT -> NUMBER
    %import common.CNAME -> NAME
    %import common.WS
    %ignore WS
"""

parser = Lark(grammar, start='start', ambiguity='explicit')

if __name__ == '__main__':
    sentence = 'a = (-2) * 3 + 5 - 1'
    ast = parser.parse(sentence)
    print(ast)
    print(ast.pretty())

    if len(sys.argv) > 1:
        tree.pydot__tree_to_png(ast, sys.argv[1])
