Exercice 3

1. ps aux | tr -s " " | tr " " ";" > processus.csv

2. ls -1t ~ | head -1

3. find ~ -iname "*.pdf" -exec basename {} \; | sort -u

4. ls ~/*.* | rev | cut -d"." -f1 | rev | sort -u

5. find ~ -iname "*.py" | grep -v "/tp1/"

6. cat /etc/profile | head -5 | tail -3

7. echo $PATH | cut -d":" -f1

8. env | wc -l

9. find ~ -type d 2> /dev/null | wc -l ; find ~ -type f 2> /dev/null | wc -l

10. ls -F ~ | grep "*$"


Exercice 4

Contenu du fichier heure.sh :

#! /bin/bash

DATE=$(date -u | cut -d',' -f2 | cut -d":" -f1,2)
echo "il est $DATE"
# OU ENCORE :
# date +"%H:%M"


Exercice 5

Contenu du fichier nettoie.sh :

#! /bin/bash

FICHIER=$1
MODIFICATION=$(date -r $FICHIER | cut -d"," -f1)

echo "Le fichier $1 a été modifié le $MODIFICATION"
read -p "Voulez vous le supprimer [o/N] ?" REPONSE

if [ "$REPONSE" != "o" ];
then
	echo "fichier conservé"
else
	rm $1
	echo "fichier supprimé"
fi

exit 0
