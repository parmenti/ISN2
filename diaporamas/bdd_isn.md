 <span class="menu-title" style="display: none">Titre</span>
![ESPE](images/logo_ul_espe_fond_transparent300dpi.png)<!-- .element: class="plain" style="width:60%;float:left;"--> 
![LORIA](images/logo_loria.png)<!-- .element: class="plain" style="width:12%;float:right;"--> 
<br><br><br><br><br><br>

---

**<br>Formation ISN - Les bases de données relationnelles<br>**

<br>_14 Mars 2019_<br><br>



# Thème du cours
<br>
Bases de données relationnelles<br><br>

> Collection de données structurée <br>organisée en tables (aka relations)<!-- .element: class="fragment" data-fragment-index="2" -->

<!-- https://www.webucator.com/tutorial/learn-sql/relational-database-basics.cfm -->



# Plan
<br>
1. Introduction aux _bases de données_<br>

2. Introduction au langage _SQL_ (_Structured Query Language_)<br>

3. Introduction au système _SQLite_<br>

4. Introduction à l'API Python _SQLalchemy_<br>



# <br><br><br>Introduction aux bases de données
<br>
Terminologie / Structure / Propriétés



# Introduction aux bases de données
<br>
- **Base de données relationnelle** (Codd, 1970):
  - _modèle_ (définition de la structure des données) 
  - _tables_ (valeur des données)

- **Tables** sont comparables à des feuilles de calcul, où
  - **lignes** (aka _enregistrements_) sont identifiées de manière unique
  - **colonnes** (aka _champs_) sont typées

- Implémentation:
  - **Système de Gestion de Bases de Données** (SGBD)



# Introduction aux bases de données (suite)
<br>
**Modèle** : partage d'information (non-redondance) <br><br>&#x27a1; tables jointes

<center>
![table](images/table.png)<!-- .element: class="plain" style="width:50%;" -->

actor&#x5f;film&#x5f;mapping &#x2b0c; **relation** _jouer-dans_
</center>
<!-- <small style="float:right;">(Relational Model using joined table)</small> -->



# Introduction aux bases de données (suite)
<br>
**Table**: exemple de la table _actor_
<br><br><br>

|actor&#x005f;id|name |
|:----|:----|
|1 | John Wayne|
|2 | Steve McQueen |
|3 | Humphrey Bogart |

<br><br>Dans le SGBD, l'identifiant peut être incrémenté automatiquement



# Introduction aux bases de données (suite)
<br>
- Identifiant de ligne _interne_ obligatoire <br>&#x2b95; **clé primaire** (_peut être atomique ou non, incrémentée automatiquement ou non_)

- Champ(s) en lien avec des identifiants _externes_ <br>&#x2b95; **clé(s) étrangère(s)**

- _Règles d'or_: les clés primaires sont non nulles, les valeurs de champ sont atomiques, les champs qui n'appartiennent pas à des clés primaires doivent être complètement dépendant des clés primaires (et seulement de celles-ci), etc.

&#x2b95; Mise sous forme(s) normale(s)

Notes: http://www.ntu.edu.sg/home/ehchua/programming/sql/relational_database_design.html



# Introduction aux bases de données (suite)
<br>
- La _conception du modèle de données_ requiert l'**analyse** de flux d'informations et de procédés (méthodologie nécessaire, par exemple MERISE)

- Plusieurs étapes de raffinement sont appliquées au modèle pour garantir:

  - **la non-redondance des données** 
  - **l'integrité des données** 
  - **la précision des données**



# <br><br><br>Introduction au langage SQL
<br>
Le langage / les interpréteurs



# Introduction au langage SQL
<br>
- Langage de type _Domain-Specific Language_ (**DSL**)

- Utilisé principalement pour **manipuler** (stocker/extraire) de l'**information** des tables d'une base de données

-  **norme** ANSI/ISO depuis 1986 (revisée in 2016)

- Langage utilisé dans plusieurs **SGBD** (PostgreSQL, Oracle, MySQL, ...)

- Langage de programmation **déclaratif** 

- Plusieurs **interpréteurs** sont disponibles selon le SGBD utilisé (SQL+, TOAD, ...)



# Introduction au langage SQL (suite)
<br>
- Types de données disponibles nativement :

 - Character strings (variable size): ```VARCHAR2(n)```

 - Binary strings: ```VARBINARY(n)```

 - Boolean values: ```BOOLEAN```

 - Numerical values: ```INTEGER | FLOAT | DECIMAL(precision, scale)```

 - Temporal values (date times): <br>```DATE | TIME | TIMESTAMP```

 - Empty value: `NULL`



# Introduction au langage SQL (suite)
<br>
Anatomie d'une **requête** SQL:

```
SELECT <columns> [AS <names>]

FROM <table> [<alias>]

[JOIN <table> ON <column> | NATURAL JOIN <table>]

[WHERE <predicate on rows>]

[GROUP BY <columns> [HAVING <predicate on groups>]]

[ORDER BY <columns> [DESC | ASC]]
```

<br>NB: 
  - SQL n'est pas sensible à la casse
  
  - les expressions entre [ ] sont optionnelles

Notes: https://en.wikipedia.org/wiki/SQL_syntax



# Introduction au langage SQL (suite)
<br>
- Lister tous les acteurs dont le nom commence par "W" (dans l'ordre alphabétique) :

```
SELECT *
 FROM  actor
 WHERE name LIKE 'W%'
 ORDER BY name;
```

- Résultat (réponse à la requête) retournée par le système :

```
id_actor  name
--------- -----------
1         Wayne, John
```



# Introduction au langage SQL (suite)
<br>
- Lister tous les acteurs ainsi que le nombre de films dans lesquels ils jouent :

```
SELECT name, count(*) AS nb_movies
 FROM actor a, actor_film_mapping play_in
 WHERE a.actor_id = play_in.actor_id 
 GROUP BY name;
```
- Résultat :

```
name                   nb_movies
---------------------- ---------
Wayne, John            2
```



### Introduction au langage SQL (suite)
<br>
- Sous-requêtes (apparaissent dans les contraintes de la clause WHERE) :

```
SELECT isbn,
       title,
       price
 FROM  Book
 WHERE price < (SELECT AVG(price) FROM Book)
 ORDER BY title;
```
- Tables dérivées (apparaissent dans la clause FROM/JOIN) :

```
SELECT b.title, b.price, sales.items_sold, sales.company_nm
FROM Book b
  JOIN (SELECT SUM(Items_Sold) Items_Sold, Company_Nm, ISBN
        FROM Book_Sales
        GROUP BY Company_Nm, ISBN) sales
  ON sales.isbn = b.isbn
```

Notes: https://swcarpentry.github.io/sql-novice-survey/



# Introduction au langage SQL (suite)
<br>
- Insérer / supprimer / mettre à jour des enregistrements :

```
INSERT INTO <table>
 (<column1>, <column2>, <column3>)
 VALUES
 (<val_col1>, <val_col2>, <val_col3>);

DELETE FROM <table>
 WHERE <column> = <value>;

UPDATE <table>
 SET <column1> = <value1>
 WHERE <column2> = <value2>;
```

<br>Les textes entre < > sont à remplacer par des valeurs compatibles



# Introduction au langage SQL (suite)
<br>
- Définition de données 
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(création / altération / suppression de tables) :

```
CREATE TABLE <table> (
 column1 <type1>,
 column2 <type2>,
 column3 <type3> [NOT NULL],
 PRIMARY KEY (<column1>, <column2>)
);

ALTER TABLE <table> ADD <column4> <type4> [NOT NULL];

DROP TABLE <table>;
```



### Introduction au langage SQL (suite)
<br>
- Il est aussi possible de contrôler les **transactions** :

```
CREATE TABLE tbl_1(id int);

 INSERT INTO tbl_1(id) VALUES(1);
 INSERT INTO tbl_1(id) VALUES(2);

COMMIT;

UPDATE tbl_1 SET id=200 WHERE id=1;

SAVEPOINT id_1upd;

UPDATE tbl_1 SET id=1000 WHERE id=2;

ROLLBACK to id_1upd;

 SELECT id from tbl_1;
```

<br>NB: c'est une notion avancée que l'on rencontre relativement peu souvent



# <br><br><br>Introduction à SQLite
<br>
Définition / Propriétés / Usages
<center>
![SQLite](images/sqlite.jpg)<!-- .element: class="plain" -->
</center>



# Introduction à SQLite
<br>
- [SQLite](https://www.sqlite.org/) est un SGBD **léger** :

 - auto-contenu (developpé en ANSI-C)

 - sans serveur

 - sans configuration

 - transactionnel

 - utilise le typage _dynamique_ 

 - peut travailler avec des bases de données _en-mémoire_ (pour des accès rapides) 
 
Notes: http://www.sqlitetutorial.net/what-is-sqlite/



# Introduction à SQLite (suite)

- Une fois installé, SQLite peut être utilisé de deux façons :
 - Via un interpréteur interactif (+ un [navigateur de BDD](https://sqlitebrowser.org/)):
 
 ```bash
 $ sqlite3 file.db
 SQLite version 3.22.0 2018-01-22 18:45:57
 Enter ".help" for usage hints
 sqlite> 
 ```

 - Via un outil graphique intégré : [SQL lite studio](https://sqlitestudio.pl/)
<center>
![](http://www.sqlitetutorial.net/wp-content/uploads/2015/11/run-sqlite-studio.jpg)<!-- .element: class="plain" style="width:30%;" -->
</center>



# Introduction à SQLite (suite)

Vue d'ensemble du tutoriel SQLite : <br><br>a) **Modèle de données** &#x21e9; &nbsp;&nbsp;&nbsp; b) **Jeu de données** [(lien)](http://www.sqlitetutorial.net/wp-content/uploads/2018/03/chinook.zip)

![](http://www.sqlitetutorial.net/wp-content/uploads/2015/11/sqlite-sample-database-color.jpg)<!-- .element: class="plain" -->



<section data-background-iframe="https://nbviewer.jupyter.org/urls/espe-lorraine.gitlab.io/ISN2/notebooks/SQLite.ipynb" data-background-interactive>
</section>



# <br><br><br>Introduction à SQLalchemy
<br>
Aligneur (mapper) d'Objets relationels 
<center>
![](https://www.fullstackpython.com/img/logos/sqlalchemy.jpg)<!-- .element: class="plain" -->
</center>



# Introduction à SQLalchemy

- **Comment exécuter des requêtes SQL depuis du code Python ?**

 - Réponse \#1: en utilisant la librairie (API) *native* [sqlite3](https://docs.python.org/3/library/sqlite3.html)*

```python
import sqlite3
conn = sqlite3.connect('example.db')
c = conn.cursor()

# Create table
c.execute('''CREATE TABLE stocks
             (date text, trans text, symbol text, qty real, price real)''')

# Insert a row of data
c.execute("INSERT INTO stocks VALUES ('2006-01-05','BUY','RHAT',100,35.14)")

# Save (commit) the changes and then close the connection
conn.commit()
conn.close()
```



# Introduction à SQLalchemy
<br>
 &#x27a1; danger: injection SQL
 <center>
![](https://imgs.xkcd.com/comics/exploits_of_a_mom.png) <!-- .element: class="plain" -->
</center>
<small style="float:right;">(from [https://xkcd.com/327/](https://xkcd.com/327/))</small>



# Introduction à SQLalchemy (suite)

- **Comment exécuter des requêtes SQL depuis du code Python ?**

 - Réponse \#2: utiliser un aligneur d'_Objets Relationels_ 

```python
#file base.py
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
 
Base = declarative_base() 
 
class Actor(Base):
  __tablename__ = 'actor'
  # Each column is also a python instance attribute
  actor_id = Column(Integer, primary_key=True)
  name     = Column(String(250), nullable=False)
 
class Film(Base):
  __tablename__ = 'film'
  film_id = Column(Integer, primary_key=True)
  title   = Column(String(250), nullable=False)
  gender  = Column(String(250))

class Actor_film_mapping(Base):
  __tablename__ = 'actor_film_mapping'
  actor_id = Column(Integer, ForeignKey('actor.actor_id'))
  film_id  = Column(Integer, ForeignKey('film.film_id'))	
  actor = relationship(Actor)
  film  = relationship(Film)
 
# Create an engine that stores data in the local 
# directory's example.db file.
engine = create_engine('sqlite:///example.db')
 
# Create all tables in the engine. This is equivalent 
# to "Create Table" statements in raw SQL.
Base.metadata.create_all(engine)
```



# Introduction à SQLalchemy (suite)
<br>
- **Librairie Python**  (API) fournissant une interface de haut-niveau (abstraction) aux bases de données relationnelles

- **Tables** sont alignées sur des _classes_ Python
- **Enregistrements** sont alignés sur des _objects_ Python

- **Alignements** sont créés **semi-automatiquement** au moyen d'objets de type `declarative_base` de l'API SQLalchemy

Notes: https://www.pythoncentral.io/introductory-tutorial-python-sqlalchemy/



# Introduction à SQLalchemy (suite)
<br>
- Comment utiliser les tables ainsi générées ?

```
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
 
from base import Actor, Film, Actor_film_mapping
 
engine = create_engine('sqlite:///example.db')
# Bind the engine to the metadata of the Base 
# class from base.py 
Base.metadata.bind = engine
 
DBSession = sessionmaker(bind=engine)
session = DBSession()
 
# Insert an actor in the actor table
actor1 = Actor(name='Smith, John')
session.add(actor1)
session.commit()

...

# In case you do not want to keep modifications made 
# since the last commit, you can revert all of them 
# back by calling
# session.rollback()
```



# Introduction à SQLalchemy (suite)
<br>
- Comment sélectionner tous les films où joue un acteur ?

```
from base import Actor, Film, Actor_film_mapping
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

engine = create_engine('sqlite:///example.db')

Base.metadata.bind = engine
DBSession = sessionmaker()
DBSession.bind = engine
session = DBSession()

actor1 = session.query(Actor).first()
print(actor1.name) #'Smith, John'

session.query(Actor_film_mapping).filter(Actor_film_mapping.actor_id == actor1).all()
[<base.Actor_Film_mapping object at 0x2ee3cd0>]
```



<section data-background-iframe="https://nbviewer.jupyter.org/urls/espe-lorraine.gitlab.io/ISN2/notebooks/SQLalchemy.ipynb" data-background-interactive>
</section>



# <br><br><br><br>Merci pour votre attention

Notes: https://towardsdatascience.com/sqlalchemy-python-tutorial-79a577141a91 https://www.oreilly.com/library/view/essential-sqlalchemy/9780596516147/ch01.html
