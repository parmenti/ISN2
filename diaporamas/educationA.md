 <span class="menu-title" style="display: none">Projet PIAF</span>
![ESPE](images/logo_ul_espe_fond_transparent300dpi.png)<!-- .element: class="plain" style="width:60%;float:left;"--> 
![LORIA](images/logo_loria.png)<!-- .element: class="plain" style="width:12%;float:right;"--> 
<br><br><br><br><br><br>

---

<br><br>_Enseigner les sciences numériques :_ <center>_comprendre pour aider à apprendre_</center>
<br><br><br>
Yannick Parmentier, séminaire « Education à », 13 Mars 2019



# <br><br><br><br><br> Introduction



# À propos du numérique
<br>
- Quelques caractéristiques :

  1. Une place grandissante dans notre vie<!-- .element: class="fragment" data-fragment-index="2" -->

Notes: comment s'en convaincre : données du "baromètre du numérique"



<section>
<h2>Usage du téléphone pour naviguer sur internet (par tranche d'âge)</h2><br>
 <canvas class="stretch" data-chart="bar">
, 2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017,2018
12-17 ans, 10.69767441860465, 9.523809523809524, 15.09433962264151, 19.63470319634703, 27.64976958525346, 49.029126213592235, 57.28155339805825, 69.15422885572139, 80.48780487804879, 83.4862385321101, 86.82926829268293, 84.5
18-24 ans, 12.0, 11.48936170212766, 26.666666666666668, 29.535864978902953, 46.42857142857143, 59.817351598173516, 77.9591836734694, 81.85840707964603, 90.3061224489796, 90.0, 94.97716894977168, 96.38009049773756
25-39 ans, 7.74526678141136, 8.097928436911488, 16.91449814126394, 20.5078125, 36.08445297504799, 43.57429718875502, 56.28997867803838, 65.51020408163265, 79.53586497890295, 80.63829787234043, 89.1156462585034, 88.20861678004536
40-59 ans, 3.816793893129771, 2.6206896551724137, 6.078147612156296, 6.419400855920114, 12.177650429799428, 24.162011173184357, 29.069767441860467, 36.29842180774749, 46.1212976022567, 52.75908479138627, 66.14509246088194, 64.39169139465875
60-69 ans,0.4273504273504274, 1.680672268907563, 1.4925373134328357, 2.127659574468085, 5.762711864406779, 7.380073800738007, 15.755627009646304, 20.0, 24.516129032258064, 23.58490566037736, 34.394904458598724, 32.0
70+ ans, 0.3448275862068966, 0.0, 0.34965034965034963, 0.35842293906810035, 1.7482517482517483, 2.7027027027027026, 3.7162162162162162, 4.318936877076411, 8.571428571428571, 10.695187165775401, 16.819571865443425, 17.98780487804878
</canvas>
<p style="float:right;font-size:14px;">Données : [baromètre du numérique](https://www.data.gouv.fr/fr/datasets/barometre-du-numerique/)</p>
<aside class="notes">
Notes: internet (donc numérique) s'invite de plus en plus dans notre vie
</aside>
</section>



<section>
<h2>Démarches administratives réalisées par internet (par tranche d'âge)</h2>
<br>
 <canvas class="stretch" data-chart="bar">
, 2009,2010,2011,2012,2013,2014,2015,2016,2017, 2018
12-17 ans, 6.60377358490566, 6.8493150684931505, 7.834101382488479, 11.165048543689322, 6.310679611650485, 5.472636815920398, 5.853658536585367, 10.091743119266056, 24.878048780487806, 16.5
18-24 ans, 48.44444444444444, 54.85232067510548, 63.83928571428571, 54.794520547945204, 64.89795918367346, 55.309734513274336, 62.244897959183675, 69.47368421052632, 81.7351598173516, 76.92307692307693
25-39 ans, 65.61338289962825, 69.140625, 70.44145873320538, 72.48995983935743, 77.18550106609808, 78.77551020408163, 79.11392405063292, 87.65957446808511, 88.88888888888889, 88.88888888888889
40-59 ans, 46.59913169319827, 51.497860199714694, 55.44412607449857, 57.960893854748605, 61.04651162790697, 59.11047345767575, 63.32863187588153, 72.94751009421265, 78.66287339971551, 76.26112759643917
60-69 ans, 30.970149253731343, 29.78723404255319, 36.94915254237288, 30.627306273062732, 41.80064308681672, 49.50819672131148, 48.70967741935484, 54.40251572327044, 57.64331210191082, 55.14285714285714
70+ ans, 5.944055944055944, 8.60215053763441, 8.741258741258742, 12.162162162162163, 13.175675675675674, 16.943521594684384, 20.0, 27.540106951871657, 31.804281345565748, 32.6219512195122
</canvas>
<p style="float:right;font-size:14px;">Données : [baromètre du numérique](https://www.data.gouv.fr/fr/datasets/barometre-du-numerique/)</p>
<aside class="notes">
Notes: intrusion volontaire ou subie
</aside>
</section>



<section>
<h2>Démarches administratives réalisées par internet (par niveau de diplôme)</h2>
<br>
 <canvas class="stretch" data-chart="bar">
, 2009,2010,2011,2012,2013,2014,2015,2016,2017,2018
sans diplôme, 14.15929203539823, 15.06172839506173, 17.892156862745097, 18.37270341207349, 21.867321867321866, 21.54696132596685, 19.018404907975462, 29.360465116279073, 30.48780487804878, 26.36986301369863
niveau Brevet, 38.89618922470434, 40.234375, 42.513368983957214, 43.77510040160642, 46.98972099853157, 47.646219686162624, 49.93234100135318, 58.45539280958722, 65.0, 63.04044630404463
niveau Bac, 55.19287833827893, 60.11080332409973, 68.6046511627907, 64.04833836858006, 66.66666666666666, 67.79661016949152, 72.95918367346938, 76.01010101010101, 82.7250608272506, 78.57142857142857
diplôme du supérieur, 73.7991266375546, 76.72955974842768, 77.09923664122137, 75.04621072088725, 82.09219858156028, 78.57142857142857, 80.98720292504571, 86.25827814569537, 89.25619834710744, 87.5195007800312
</canvas>
<p style="float:right;font-size:14px;">Données : [baromètre du numérique](https://www.data.gouv.fr/fr/datasets/barometre-du-numerique/)</p>
<aside class="notes">
Notes: pas tous égaux devant cette intrusion
</aside>
</section>



# À propos du numérique (suite)
<br>
- Quelques caractéristiques :

  1. Une place grandissante (et inégale) dans notre vie

  2. Des applications toujours plus nombreuses et<br> plus puissantes (complexité accrue)<!-- .element: class="fragment" data-fragment-index="2" -->




<section tagcloud large> <span class="menu-title" style="display: none">Quelques applications du numérique</span>
<br><br>
    Finance
	Chirurgie
	Météorologie
	Traduction
	Recherche d'information
	Communication
	Information & média
	Jeux
	Transports
	Médecine
	Art
	Loisirs
	Education
	Ecologie
	Energie
</section>



# À propos du numérique (suite)
<br>
- Quelques caractéristiques :

  1. Une place grandissante (et inégale) dans notre vie

  2. Des applications toujours plus nombreuses et<br> plus puissantes (complexité accrue)
  
  3. Des interfaces toujours plus simples<!-- .element: class="fragment" data-fragment-index="2" -->

Notes: paradoxe complexité des traitements / simplicité des interfaces



# &Eacute;volution des interfaces
<br>
![avant](https://upload.wikimedia.org/wikipedia/en/e/e2/Pcpaint_000.png)<!-- .element: style="width:35%;float:left;" -->
![apres](https://cdn.mos.cms.futurecdn.net/pjHhNJT7mASk5v6U3Xrt5Q-650-80.jpg)<!-- .element: style="width:35%;float:right;" -->

<br><br><br><br><br>

![avant2](https://upload.wikimedia.org/wikipedia/commons/7/74/THinkPad345C.jpg)<!-- .element: style="width:35%;float:left;" class="fragment" data-fragment-index="2" -->
![apres2](https://upload.wikimedia.org/wikipedia/commons/f/fe/Nexus_10.png)<!-- .element: style="width:35%;float:right;" class="fragment" data-fragment-index="2" -->

Notes: paradoxe s'applique tant au logiciel qu'au matériel



# À propos du numérique
<br>
- Quelques caractéristiques :

  1. Une place grandissante (et inégale) dans notre vie

  2. Des applications toujours plus nombreuses et<br> plus puissantes (complexité accrue)
  
  3. Des interfaces toujours plus simples
  
  4. Un nouveau rapport au temps, à l'espace<!-- .element: class="fragment" data-fragment-index="2" -->
  
  5. Une découverte récente et en rapide évolution<!-- .element: class="fragment" data-fragment-index="3" -->

Notes: info en temps réel / accessible partout, notion de véracité/confiance remise en cause



# La révolution numérique
<iframe data-src='https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=10nNzlGueus--UTlukalRMG1ndSLHkUu12LsSCdZvKCU&font=Default&lang=en&initial_zoom=2&height=650' width='100%' height='650' webkitallowfullscreen mozallowfullscreen allowfullscreen frameborder='0'></iframe>



# En résumé

|    |    |    |    |     |     |    |
|:---|:--:|:--:|:--:|:---:|:---:|---:|
|-5000|   |1000|  |1450| |1537|
|![ecriture](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Wadi_el-Hol_inscriptions_drawing.jpg/280px-Wadi_el-Hol_inscriptions_drawing.jpg)<!-- .element: style="width:100%;" class="plain" -->| |![gravure](https://upload.wikimedia.org/wikipedia/commons/4/48/%E4%BA%94%E8%B4%AF%E5%AE%9D%E5%8D%B7.jpg)<!-- .element: style="width:35%;" class="plain" -->| |![gravure](https://upload.wikimedia.org/wikipedia/commons/2/27/Gutenberg_bible_Old_Testament_Epistle_of_St_Jerome.jpg)<!-- .element: class="plain" style="width:21%" -->| | ![dépôt légal](https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Fran%C3%A7ois_Ier_Louvre.jpg/220px-Fran%C3%A7ois_Ier_Louvre.jpg)<!-- .element: style="width:65%;" class="plain" -->|
|<br>| | | | | | |
|1703|   |1937| |1965| |1969|
|![bits](https://upload.wikimedia.org/wikipedia/commons/a/ac/Leibniz_binary_system_1703.png)<!-- .element: style="width:70%;" class="plain" -->| |![computer](https://upload.wikimedia.org/wikipedia/commons/0/01/Atanasoff-Berry_Computer_at_Durhum_Center.jpg)<!-- .element: style="width:50%;" class="plain" -->| |![pc](https://upload.wikimedia.org/wikipedia/commons/5/5e/Olivetti_Programma_101_-_Museo_scienza_e_tecnologia_Milano.jpg)<!-- .element: style="width:45%;" class="plain" -->| |![internet](https://upload.wikimedia.org/wikipedia/commons/b/bf/Arpanet_logical_map%2C_march_1977.png)<!-- .element: style="width:100%;" class="plain" -->|



# <br><br><br><br><br> Apprendre <font color="grey">(avec ?)</font> le numérique



# Apprendre (avec ?) le numérique
<br>
 - 0 Qu'est ce que le numérique ?<!-- .element: class="fragment" data-fragment-index="2" -->
<br><p class="fragment" data-fragment-index="2">**numérique** = outils logiciels / matériels + données (binaires) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ normes + *usages*</p>

 - 1 Doit on apprendre à <strike>utiliser</strike> appréhender le numérique ? 
 
 - 2 Est ce le rôle de l'&Eacute;cole d'initier cet apprentissage ?
 
 - 3 Comment faire ?

Notes: lien entre outils, données, normes et usages sous-estimé, littéracie := connaissances fondamentales dans les domaines de la lecture et de l’écriture, permettant à une personne d’être fonctionnelle en société.



# Appréhender le numérique
<br>
- Appréhender le numérique &#x21d2; Apprendre l'informatique ?<br><br>

- Il est important d'avoir de « bonnes » images<br><br><!-- .element: class="fragment" data-fragment-index="2" -->

- On peut commencer tôt à se construire ces images <br>(en particulier entre 5 et 12 ans) <!-- .element: class="fragment" data-fragment-index="3" -->

  <p class="fragment" data-fragment-index="3"><br>$\rightarrow$ cela ne nécessite pas forcément de matériel informatique <br>&nbsp;&nbsp;&nbsp;ni une grande expérience de l'informatique (à suivre)</p>

Notes: question ouverte, dépendance numérique



# Histoire de l'informatique à l'école

<p>**Années 70 :**

  <br>- (1970) introduction de l'**informatique (outil) dans le secondaire** par la pratique du langage de programmation *Logo*</p>

<p class="fragment" data-fragment-index="2">**Années 80 :**

  <br>- (1985) plan **informatique pour tous** avec l'équipement de 50000 établissements scolaires (*120000 machines*) et apprentissage des langages *Logo* et *BASIC*
  
  <br>- (1987) introduction d'une **option d'informatique** au baccalauréat</p>  

<p class="fragment" data-fragment-index="3">**Années 90 :**

  <br>- (1992) **suppression de l'option informatique** au baccalauréat</p>

<p class="fragment" data-fragment-index="4">**Années 2000 :**

  <br>- (2002) **création du brevet informatique et internet**</p>



# L'informatique à l'école de nos jours

<p>**Années 2010 :**

  <br>- (2011) Nouveau référentiel du **certificat informatique et internet**
  <br>- (2015) **Algorithmique** intégrée au programme du cycle 4
  <br>- (2018) Mise en ligne du portail national **pix.fr** intitulé<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; « Cultivez vos compétences numériques »</p>

<p class="fragment" data-fragment-index="2">**A noter:**</p>

  <p class="fragment" data-fragment-index="2">$\rightarrow$ Le terme _informatique_ est peu à peu remplacé par _numérique_ <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(cf référentiel européen DigComp, _digital competencies_)</p>
  
  <p class="fragment" data-fragment-index="2">$\rightarrow$ On se dirige vers une distinction entre :</p>
  <p class="fragment" data-fragment-index="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**numérique** (outil) enseigné à tous, et </p>
  <p class="fragment" data-fragment-index="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**algorithmique** (science) enseignée au sein des mathématiques</p>



# <br><br><br><br><br> Pendant ce temps là aux États-Unis



# Naissance de la _pensée informatique_
<br>
> Penser informatiquement, c'est utiliser l'abstraction et la
décomposition quand il s'agit d'affronter une tâche large et complexe
ou quand il s'agit de concevoir un système large et complexe ; c'est
séparer les différents aspects ; c'est choisir une représentation
appropriée pour un problème ou modéliser les aspects pertinents d'un
problème pour les rendre accessible (...)

<p style="float:right;font-size:12;">Jeannette Wing (2006)</p>



# Définition de référentiels aux USA

- [CSTA Standards](https://drive.google.com/file/d/13QUISBtrOVfpbJcEJD8_7ibJqxuGvx1r/view)
   <br><br>
   <center>
   <iframe src="https://drive.google.com/file/d/13QUISBtrOVfpbJcEJD8_7ibJqxuGvx1r/preview" width="940" height="480"></iframe>
   </center>



# En Australie

- [ACARA](https://docs.acara.edu.au/resources/Digital_Technologies_-_Sequence_of_content.pdf)
   <br><br>
   <center>
   <iframe src="https://docs.acara.edu.au/resources/Digital_Technologies_-_Sequence_of_content.pdf" width="940" height="480"></iframe>
   </center>



# Au Royaume-Uni

- [Teaching London Computing (UK)](https://teachinglondoncomputing.files.wordpress.com/2014/07/computing_progression_pathways_with_computational_thinking_v2-3.pdf)
   <br><br>
   <center>
   <iframe src="https://teachinglondoncomputing.files.wordpress.com/2014/07/computing_progression_pathways_with_computational_thinking_v2-3.pdf" width="940" height="480"></iframe>
   </center>



# En Europe

- [DigComp (EU)](https://joint-research-centre.ec.europa.eu/digcomp/digcomp-framework_en)
   <br><br>
   <center>
   <iframe src="digcomp2_1.pdf" width="940" height="480"></iframe>
   </center>



# <br><br><br><br><br> Et en France



# Propositions de programme
<center>
![](images/science_informatique_transparent.png)<!-- .element: class="plain" -->
</center>
Voir par ailleurs (Dowek, 2011) pour la spécialité "Informatique et Sciences du Numériques" en Terminale S 



# Contexte du 1er degré

<p style="float:left;width:35%;">
- Des enseignant.e.s principalement issus de SHS
<br><br>_- Des personnes avec une expérience propre du numérique (heureuse ou non)_<!-- .element: class="fragment" data-fragment-index="2" -->
<br><br>_- Des personnes avec une image propre du numérique (e.g. genre, technicité, fiabilité) <br>[Collet, 2011]_<!-- .element: class="fragment" data-fragment-index="3" -->
</p>

<p style="float:right;width:65%;">
<canvas style="float:right;" class="stretch" data-chart="pie">
,Langues (11.4%), Arts lettres et sciences du langage (13.9%), Sciences humaines et sociales (46.1%), S.T.A.P.S. (7.2%), Sciences (14.3%),Droit (2%),Economie (4.2%)
MEEF 1er degré, 11.4, 13.9, 46.1, 7.2, 14.3, 2, 4.2
center, 0,0,0,0
</canvas>
&nbsp;<br><span style="font-size:12pt;float:right;">Source: [MESRI-DGESIP-DGRI SIES](http://www.enseignementsup-recherche.gouv.fr/cid130761/les-effectifs-en-espe-en-2017-2018.html) / <br>Système d’information SISE (2017-2018)</span>
</p>

Notes: Pré-requis : formation des Professeur.e.s des Ecoles


# Les sciences numériques et le genre
<br>
<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/3S5BLs51yDQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>



# Enjeux de la formation
<br>
- Permettre aux (futur.e.s) enseignant.e.s d'acquérir des bases en (pensée) informatique et de se construire de _bonnes_ images

- <p>Convaincre ce public que la pensée informatique est <b>accessible</b></p><!-- .element: class="fragment" data-fragment-index="2" -->

  - <p>en terme de <b>compétences</b></p><!-- .element: class="fragment" data-fragment-index="3" -->
  - <p>en terme de <b>coûts</b></p><!-- .element: class="fragment" data-fragment-index="3" -->

- <p class="fragment" data-fragment-index="4">Mise en oeuvre : informatique « débranchée » (Bell et al, 2005)</p>

Notes: Delarbre (2011) qu'une formation à l'informatique débranchée réduisait significativement l'appréhension pour l'algorithmique chez les enseignant.e.s. Beaucoup de ressources éprouvées existent (cf [Bell & Newton, 2013]).



# Quelles formations viser ?
<br>
- <p>Enseignant.e.s en postes (formation continue)</p>
<center>
![MSCVL](https://centre-valdeloire.maisons-pour-la-science.org/sites/default/files/logo_mpls/MPLS-CENTRE-VAL-DE-LOIRE-QUADRI.svg)<!-- .element: class="plain" style="width:30%;" -->
</center>
- <p>Futur.e.s enseignant.e.s (formation initiale)</p>
<center>
![INSPE](https://inspe.univ-lorraine.fr/themes/custom/projet/images/logos/inspe.png)<!-- .element: class="plain" -->
</center>



# 3 types d'activités utilisées
<p style="width:50%;float:left;">
**Axe A** 
(science des algorithmes)
<br><br><br>- réseau de tri
   
<br><br><br><br><br>- tri d'objets
<br>![](images/roberval.png)<!-- .element: class="plain" style="float:right;" -->
</p>
<p style="width:40%;float:right;">
![](images/tri.jpg)<!-- .element: class="plain" style="float:right;" -->
<br>
![](images/tri1.png)<!-- .element: class="plain" style="float:right;" -->
</p>



# 3 types d'activités utilisées (suite)
<p style="width:50%;float:left;">
**Axe B** 
- (science de la numérisation)
<br><br>- activité de représentation d'images
<br><br><br>
**Axe C**
(science de la communication)
<br><br><br>- activité du jeu de l'orange
</p>
<p style="width:45%;float:right;">
![](images/activite_images.png)<!-- .element: class="plain" style="width="100%;float:right;" -->
<br><br>
![](images/orange.png)<!-- .element: class="plain" style="float:right;" -->
</p>



# &Eacute;valuation des formations
1- A la maison pour la science (en Région Centre Val de Loire)
<br>$\rightarrow$ 145 formés (1/2 à 2 jours)
<br><br>52 participants à une enquête post-formation :

  - 84 % ont déclaré que l'action leur a apporté ce qu'ils attendaient,
  - 90 % ont déclaré que l'action a été motivante pour enseigner les sciences,
  - 96 % ont déclaré que l'action leur a permis d'acquérir de nouvelles connaissances en relation ave le thème abordé,
  - 84 % ont déclaré que l'action leur a permis d'acquérir de nouvelles compétences en lien ave l'enseignement des sciences,
  - 80 % ont déclaré qu'ils envisagent d'utiliser en classe
ce qu'ils ont appris lors de l'action



# &Eacute;valuation des formations (suite)
<br>
2- A l'ESPE de Lorraine (Master MEEF 1er degré)
<br><br>$\rightarrow$ 165 formés (2h) promo 2017-2018 site de Maxéville
<br>
<br>Retours limités :

  - une séance introductive, 
  
  - public pas encore sensible à la question du numérique



# <br><br><br><br><br> Le projet PIAF



<section data-background-iframe="https://framacarte.org/fr/map/piaf_44199" data-background-interactive>
</section>



# Le projet
<br>
<center>
<img class="stretch" src="images/summary-piaf.png"/>
</center>



# Méthodologie 
<br>
1. Conception d'un référentiel de compétences

2. Réalisation de scénarios pédagogiques pour l'acquisition de ces compétences 

3. &Eacute;valuation de l'impact de ces scénarios sur des apprenants



<section>
<h1>Proposition de référentiel (en cours)</h1>
<center>
<img scroll="yes" height="550px" src="images/CompetencesPIAmarie26_02.png"/>
</center>
</section>



# Réalisation de scénarios pédagogiques
<br>
- Plusieurs sources d'inspiration :

  - Livre [1,2,3 Codez](http://www.fondation-lamap.org/123codez) paru aux éditions Pommier (2016) 
  
  - Site [CS Unplugged](https://classic.csunplugged.org) présentant des activités débranchées rassemblées depuis 2005 (Nouvelle Zélande)
  
  - Page [Sciences Manuelles du Numériques](http://people.irisa.fr/Martin.Quinson/Mediation/SMN/)
  
  - Programmes [Coding Lab (Asie)](https://www.codinglab.com.sg/our-classes/)



# Séminaires communs

<div class="tweet" data-src="https://twitter.com/MarieKremer3/status/1102595815535771648"></div>



# Séminaires communs (suite)

<div class="tweet" data-src="https://twitter.com/MarieKremer3/status/1102570955656843264"></div>



# Conclusion
<br>
- travail préliminaire de formation de non-spécialistes à la pensée
  informatique :

  - retours positifs dans le cas de la formation ontinue (intérêt
	pratique fort de la formation)

  - retours mitigés dans le cas de la formation initiale (formation
	courte, frein psychologique)

- PIAF : approche transnationale visant à définir un cadre commun pour
  l'apprentissage de la pensée informatique et algorithmique

- évaluation des apprentissages de la pensée informatique à construire
  (Rodriguez et al., 2017)



# Conclusion (suite)
<br>
- compétences transverses (logique, mathématiques, langage, éducation civique, etc.) &#x2260; "simples" compétences d'utilisateur / programmeur informatique
<br><br>
- apprentissage compatible avec une éducation à l'exposition aux écrans et à une sensibilisation aux conséquences environnementales de la révolution numérique



# <br><br><br><br>Merci pour votre attention
<br><br>Travail réalisé en collaboration avec François Barillon, Florent Becker, Allain-Gérald Faux et Philippe Huet (Maison pour la Science d'Orléans)



# Références
<br>
Bell T., Newton H. Unplugging Computer S ien e. In Improving
Computer S ien e Edu ation , D. M. Kadijevi h, C. Angeli, C.
S hulte (eds), pp. 66-81, New York, Routledge (2013).

Bell T., Witten I. H., Fellows M. Computer Science Unplugged: An
enrichment and extension programme for primary-aged students (2005).

Collet I. Effet de genre, le paradoxe des études d'informatique. TIC &
So iété, 5(1) Retrieved from https://archive-ouverte.unige.ch/unige:18794 (2011).

Delarbre P. Initiation à la programmation à l'école primaire : les
activités « débranchées ». Mémoire de CAFIPEMF, Rectorat de
l'Académie de Lyon (2017).



# Références (suite)
<br>
Dowek G. Les quatre on epts de l'informatique. S ien es et
te hnologies de l'information et de la ommuni ation en milieu
édu atif : Analyse de pratiques et enjeux dida tiques, a tes
Didapro 4, Université de Patras (2011).

Rodriguez B., Kenni utt S., Rader C., and Camp T. Assessing
Computational Thinking in CS Unplugged Activities. In
Proceedings of the 2017 ACM Technical Symposium on
Computer Science Education (SIGCSE '17). New York, USA,
pp. 501-506 (2017).
