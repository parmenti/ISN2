


Alpha Go,2015
Arithmétique binaire (article Leibniz),1703
Arpanet,1969
Clé USB,1999
Compact Disc (audio),1979
Disquette,1967
EDSAC (premier ordinateur programmable de type Van Neumann),1949
ENIAC (premier ordinateur électronique programmable),1948
Facebook,2004
Google Car,2010
Google Inc,1998
IBM 7090 (utilisé par la NASA pour le calcul de trajectoires),1962
IPhone,2007
Machine ABC (premier ordinateur électronique non programmable),1937
Machine analytique de Charles Babbage,1834
Machine Colossus (cryptanalyse),1943
Machine de Turing,1936
Métier à tisser,1801
Norme 3G,2000
Ordinateur Programma 101 (premier PC),1965
Pascaline,1652
Premier navigateur,1993
Programme d'Ada Lovelace,1843
Souris,1963
Wifi,1997
Wikipedia,2001
Windows OS,1985
WWW,1989
